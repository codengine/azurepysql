from datetime import datetime


class Clock(object):
    @classmethod
    def read_current_datetime(cls):
        return datetime.now()
