import os
import sys
import json
from etl_enums import OperatingSystem


class ConfigManager(object):

    def detect_os(self):
        if "linux" in sys.platform:
            return OperatingSystem.LINUX.value
        elif "darwin" in sys.platform:
            return OperatingSystem.OSX.value
        elif "win" in sys.platform:
            return OperatingSystem.WINDOWS.value

    def __init__(self, config="config.json"):
        self.config = config
        self.config_json = self.load_config()

        self.os_name = self.detect_os()

        # if self.os_name:
        #     logger.log_info("Operating system detected: %s" % self.os_name)
        # else:
        #     logger.log_info("Operating system could not be detected. Default Windows will be assumed")

    def load_config(self):
        content = None
        with open(self.config, "r") as config_file:
            content = config_file.read()

        if content:
            return json.loads(content)
        else:
            return {}

    def read_database_config(self):
        if self.config_json:
            return self.config_json.get('database', {})
        return {}

    def read_server(self):
        db_config = self.read_database_config()
        return db_config.get('server')

    def read_port(self):
        db_config = self.read_database_config()
        return db_config.get('port')

    def read_database(self):
        db_config = self.read_database_config()
        return db_config.get('database')

    def read_user(self):
        db_config = self.read_database_config()
        return db_config.get('user')

    def read_password(self):
        db_config = self.read_database_config()
        return db_config.get('password')

    def read_driver(self):
        db_config = self.read_database_config()
        return db_config.get('driver')

    def read_files_config(self):
        if self.config_json:
            return self.config_json.get('files', {})
        return {}

    def read_log_config(self):
        if self.config_json:
            return self.config_json.get('log', {})
        return {}

    def read_log_file_path(self):
        log_config = self.read_log_config()
        return log_config.get("log_file")

    def read_log_file_directory(self):
        log_config = self.read_log_config()
        return log_config.get("log_directory") or os.path.join(os.getcwd(), "log")

    def read_csv_directory(self):
        files_config = self.read_files_config()
        return files_config.get('csv_directory')

    def read_temp_directory(self):
        files_config = self.read_files_config()
        return files_config.get('temp_directory')

    def read_archive_directory(self):
        files_config = self.read_files_config()
        return files_config.get('archive_directory')

    def read_config(self):
        if self.config_json:
            return self.config_json.get('config', {})
        return {}

    def read_login(self):
        if self.config_json:
            return self.config_json.get('login', {})
        return {}

    def read_config_files(self):
        cfg = self.read_config()
        return cfg.get('files', {})

    def read_row_count_file_path(self):
        cfg_files = self.read_config_files()
        file_directory = cfg_files.get("row_count_file_directory") or os.getcwd()
        file_name = cfg_files.get("row_count_file_name", "row_count.json")
        return file_directory, file_name, os.path.join(file_directory, file_name)

    def read_table_upload_status_config(self):
        cfg_files = self.read_config_files()
        file_directory = cfg_files.get("table_upload_status_directory") or os.getcwd()
        file_name = cfg_files.get("table_upload_status_file_name", "table_upload_status.json")
        return file_directory, file_name, os.path.join(file_directory, file_name)

    def read_failed_log_json_file_path(self):
        cfg_files = self.read_config_files()
        failed_rows_directory = cfg_files.get("failed_rows_json_directory") or os.getcwd()
        failed_rows_file_name = cfg_files.get("failed_rows_json_file_name", "failed_rows.json")
        return failed_rows_directory, failed_rows_file_name, os.path.join(failed_rows_directory, failed_rows_file_name)

    def read_lock_file_path(self):
        cfg_files = self.read_config_files()
        directory = cfg_files.get("lock_files_directory") or os.getcwd()
        file_name = cfg_files.get("lock_file_name", "process.lock")
        return directory, file_name, os.path.join(directory, file_name)

    def read_upload_tracker_path(self):
        cfg_files = self.read_config_files()
        directory = cfg_files.get("upload_tracker_directory") or os.getcwd()
        file_name = cfg_files.get("upload_tracker_file_name", "upload_tracker.json")
        return directory, file_name, os.path.join(directory, file_name)

    def read_download_directory(self):
        files_config = self.read_files_config()
        return files_config.get('download_directory', os.path.join(os.getcwd(), 'chambers_downloads'))

    def read_login_credentials(self):
        login = self.read_login()
        return login.get('username', ''), login.get('password', '')
