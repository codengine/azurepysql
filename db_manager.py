import pyodbc
from random import randint
import time
from etl_logger import ETLLogger
from etl_exception import ETLException
from functools import wraps

turbo_installed = False

MAX_FAILED_RETRY = 2

try:
    import turbodbc
    turbo_installed = True
except Exception as exp:
    pass


logger = ETLLogger.get_logger()


def wrap_with_exception(func):
    @wraps(func)
    def wrapper(self, **kwargs):
        try:
            return func(self, **kwargs)
        except Exception as exp:
            pass
    return wrapper


class DBManager(object):
    """
    conn = pyodbc.connect('DRIVER={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.2.so.0.1};SERVER=cmasterdata.database.windows.net,1433',
                          user='cm-user@cmasterdata', password='23wYD8TPRhSVBggd', database='cmdata')
    """
    def __init__(self, server, port, user, password, database, driver, turbo=False, timeout=1000):
        if any([not p for p in [server, port, user, password, database, driver]]):
            raise ETLException("Database configuration incorrect or missing")
        self.connection_config = {
            'server': server,
            'port': port,
            'user': user,
            'password': password,
            'database': database,
            'driver': driver
        }
        if turbo:
            if not turbo_installed:
                raise ETLException("turbodbc not installed")
        self.turbo = turbo
        self.timeout = timeout

    def build_connection_string(self):
        if self.connection_config:
            return "DRIVER={%s};SERVER=%s,%s" % (self.connection_config['driver'], self.connection_config['server'], self.connection_config['port'])

    def execute_ddl_query(self, query):
        try:
            logger.log_info("Executing DDL query: %s" % query)
            if self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DDL Query executed successfully")
                return True
            else:
                logger.log_info("Database not connected")
                return False
        except pyodbc.ProgrammingError as perr:
            logger.log_info("Query execute failed perr. Exception message: %s. Exiting now..." % str(perr))
            exit()
        except pyodbc.OperationalError as oerr:
            logger.log_info("Query execute failed. Database not connected. Exception message: %s" % str(oerr))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DDL Query executed successfully")
                return True
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()

        except SystemError as serr:
            logger.log_info("Query execute failed serr. Exception message: %s. Exiting now..." % str(serr))
            exit()
        except Exception as exp:
            logger.log_info("DDL Query execute failed. Exception message: %s" % str(exp))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DDL Query executed successfully")
                return True
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()
            return False

    def execute_query(self, query):
        data = []
        try:
            logger.log_info("Executing query: %s" % query)
            if self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                for row in cursor.fetchall():
                    data += [row]
                cursor.close()
                del cursor
                logger.log_info("Query executed successfully")
            else:
                logger.log_info("Database not connected")
        except pyodbc.ProgrammingError as perr:
            logger.log_info("Query execute failed perr. Exception message: %s. Exiting now..." % str(perr))
            exit()
        except pyodbc.OperationalError as oerr:
            logger.log_info("Query execute failed. Database not connected. Exception message: %s" % str(oerr))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                for row in cursor.fetchall():
                    data += [row]
                cursor.close()
                del cursor
                logger.log_info("Query executed successfully")
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()

        except SystemError as serr:
            logger.log_info("Query execute failed serr. Exception message: %s. Exiting now..." % str(serr))
            exit()
        except Exception as exp:
            logger.log_info("Query execute failed. Exception message: %s" % str(exp))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                for row in cursor.fetchall():
                    data += [row]
                cursor.close()
                del cursor
                logger.log_info("Query executed successfully")
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()
        return data

    def execute_query_iter(self, query):
        try:
            logger.log_info("Executing query: %s" % query)
            if self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                for row in cursor.fetchall():
                    yield row
                cursor.close()
                del cursor
                logger.log_info("Iter Query executed successfully")
            else:
                logger.log_info("Database not connected")
        except Exception as exp:
            logger.log_info("Iter Query execute failed. Exception message: %s" % str(exp))

    def insert_many(self, insert_query, data):
        cursor = None
        try:
            logger.log_info("Executing insert many query: %s" % insert_query)
            logger.log_info("insert many with data: %s" % str(data))
            if self.connection:
                cursor = self.connection.cursor()
                # cursor.autocommit = False
                cursor.fast_executemany = True
                cursor.executemany(insert_query, data)
                cursor.commit()
                # cursor.autocommit = True
                rows_affected = cursor.rowcount
                # print(rows_affected)
                cursor.close()
                del cursor
                logger.log_info("insert many Query executed successfully")
                return True, None
            else:
                logger.log_info("Database not connected")
                return False, "Database Not Connected"
        except Exception as exp:
            if cursor:
                cursor.rollback()
            logger.log_info("Execute many Query execute failed. Exception message: %s" % str(exp))
            return None, "Execute many Query execute failed. Exception message: %s" % str(exp)

    def insert_many_raw(self, insert_statement, data):
        cursor = None
        try:
            logger.log_info("insert many raw with data with size: %s" % str(len(data)))
            if self.connection:
                cursor = self.connection.cursor()
                data_string = ", ".join(data)
                query_string = "%s %s" % (insert_statement, data_string)

                # logger.log_info("========================QUERYSTRING to be executed==========================")

                # logger.log_info(query_string)

                # logger.log_info("========================END==================================================")

                cursor.execute(query_string)
                cursor.commit()
                cursor.close()
                del cursor
                logger.log_info("insert raw many Query executed successfully")
                return True, None
            else:
                logger.log_info("Database not connected")
                return False, "Database Not Connected"
        except pyodbc.ProgrammingError as perr:
            logger.log_info("Insert Raw Many Query execute failed perr. Exception message: %s" % str(perr))
            logger.log_info("Failed with data: %s" % str(data))
            # exit()
            return False, "Programming Error: %s" % str(perr)
        except pyodbc.OperationalError as oerr:
            logger.log_info("Insert Raw Many Query execute failed. Database not connected. Exception message: %s" % str(oerr))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                data_string = ", ".join(data)
                query_string = "%s %s" % (insert_statement, data_string)

                # logger.log_info("========================QUERYSTRING to be executed==========================")

                # logger.log_info(query_string)

                # logger.log_info("========================END==================================================")

                cursor.execute(query_string)
                cursor.commit()
                cursor.close()
                del cursor
                logger.log_info("insert raw many Query executed successfully")
                return True, None
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()

        except SystemError as serr:
            logger.log_info("Query execute failed serr. Exception message: %s. Exiting now..." % str(serr))
            exit()
        except Exception as exp:
            if cursor:
                cursor.rollback()
            logger.log_info("Execute many raw Query execute failed. Exception message: %s" % str(exp))
            logger.log_info("Failed with dataset: %s" % str(data))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                data_string = ", ".join(data)
                query_string = "%s %s" % (insert_statement, data_string)

                # logger.log_info("========================QUERYSTRING to be executed==========================")

                # logger.log_info(query_string)

                # logger.log_info("========================END==================================================")

                cursor.execute(query_string)
                cursor.commit()
                cursor.close()
                del cursor
                logger.log_info("insert raw many Query executed successfully")
                return True, None
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()
            return None, "Execute many raw Query execute failed. Exception message: %s" % str(exp)

    def insert_single(self, insert_query, single_row):
        cursor = None
        try:
            logger.log_info("Executing insert single query: %s" % insert_query)
            logger.log_info("insert single with data: %s" % str(single_row))
            if self.connection:
                cursor = self.connection.cursor()
                # cursor.autocommit = False
                cursor.execute(insert_query, *single_row)
                cursor.commit()
                rows_affected = cursor.rowcount
                cursor.close()
                del cursor
                logger.log_info("insert single Query executed successfully")
                return True, None
            else:
                logger.log_info("Database not connected")
                return False, "Database not connected"
        except Exception as exp:
            if cursor:
                cursor.rollback()
            logger.log_info("Execute single Query failed. Exception message: %s" % str(exp))
            return None, "Execute single Query failed. Exception message: %s" % str(exp)

    def insert_single_raw(self, insert_statement, data_raw):
        cursor = None
        try:
            logger.log_info("insert single raw with data with size: %s" % str(len(data_raw)))
            if self.connection:
                cursor = self.connection.cursor()
                query_string = "%s %s" % (insert_statement, data_raw)
                cursor.execute(query_string)
                cursor.commit()
                cursor.close()
                del cursor
                logger.log_info("insert single raw Query executed successfully")
                return True, None
            else:
                logger.log_info("Database not connected")
                return False, "Database Not Connected"
        except pyodbc.ProgrammingError as perr:
            logger.log_info("Insert Raw raw Query execute failed perr. Exception message: %s. Exiting now..." % str(perr))
            exit()
        except pyodbc.OperationalError as oerr:
            logger.log_info("Insert Raw raw Query execute failed. Database not connected. Exception message: %s" % str(oerr))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                query_string = "%s %s" % (insert_statement, data_raw)
                cursor.execute(query_string)
                cursor.commit()
                cursor.close()
                del cursor
                logger.log_info("insert single raw Query executed successfully")
                return True, None
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()

        except SystemError as serr:
            logger.log_info("Query execute failed serr. Exception message: %s. Exiting now..." % str(serr))
            exit()
        except Exception as exp:
            if cursor:
                cursor.rollback()
            logger.log_info("Execute single raw Query execute failed. Exception message: %s" % str(exp))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                try:
                    cursor = self.connection.cursor()
                    query_string = "%s %s" % (insert_statement, data_raw)
                    cursor.execute(query_string)
                    cursor.commit()
                    cursor.close()
                    del cursor
                    logger.log_info("insert single raw Query executed successfully")
                    return True, None
                except Exception as exp1:
                    return False, "Exception: %s" % str(exp1)
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()
            return None, "Execute single raw Query execute failed. Exception message: %s" % str(exp)

    def execute_dml_query(self, query):
        try:
            logger.log_info("Executing DML query: %s" % query)
            if self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DML Query executed successfully")
                return True, None
            else:
                logger.log_info("Database not connected")
                return False, "Database not connected"
        except pyodbc.ProgrammingError as perr:
            logger.log_info("Execute DML Query execute failed perr. Exception message: %s. Exiting now..." % str(perr))
            exit()
        except pyodbc.OperationalError as oerr:
            logger.log_info("Execute DML Query execute failed. Database not connected. Exception message: %s" % str(oerr))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DML Query executed successfully")
                return True, None
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()

        except SystemError as serr:
            logger.log_info("Query execute failed serr. Exception message: %s. Exiting now..." % str(serr))
            exit()
        except Exception as exp:
            logger.log_info("DML Query execute failed. Exception message: %s" % str(exp))
            self.connect(attempt=0)
            # If the code reach here that means database connected.
            if self.connected and self.connection:
                cursor = self.connection.cursor()
                cursor.execute(query)
                self.connection.commit()
                cursor.close()
                del cursor
                logger.log_info("DML Query executed successfully")
                return True, None
            else:
                logger.log_info("Database connection failed. Exiting now ...")
                exit()
            return False, str(exp)

    def close(self):
        logger.log_info("Trying to close the database connection")
        if self.connection:
            try:
                self.connection.close()
                logger.log_info("Database connection closed")
            except Exception as exp:
                logger.log_info("Database connection close failed. Exception message: %s" % str(exp))
        else:
            logger.log_info("Database not connected")
        self.connection = None
        self.connected = False

    def build_turboc_connection_string(self):
        return "Server=%s;Database=%s;User Id=%s;Password=%s;" \
               % (self.connection_config.get("server", ""), self.connection_config.get("database", ""),
                  self.connection_config.get("user", ""), self.connection_config.get("password", ""))

    def connect(self, attempt=0):
        try:
            logger.log_info("Trying to create the database connection")
            connection_string = self.build_connection_string()
            if connection_string:
                user = self.connection_config['user']
                password = self.connection_config['password']
                database = self.connection_config['database']
                if self.turbo:
                    conn_cfg = {
                        "driver" : self.connection_config.get("driver"),
                        "port" : self.connection_config.get("port"),
                        "server" : self.connection_config.get("server"),
                        "user" : user,
                        "password": password,
                        "database": database
                    }
                    self.connection = turbodbc.connect(**conn_cfg)
                    # self.connection.setencoding('utf-8')
                else:
                    self.connection = pyodbc.connect(connection_string,
                                                     user=user, password=password, database=database,
                                                     autocommit=True,
                                                     timeout=self.timeout)
                    self.connection.timeout = self.timeout
                    # self.connection.setencoding('utf-8')
                    # self.connection.setencoding(encoding='latin1')
                self.connected = True
            else:
                self.connection = None
                self.connected = False
            logger.log_info("Database connection successful")
            return self
        except SystemError as serr:
            logger.log_info("System Error detected")
            exit()
        except pyodbc.OperationalError as oerr:
            if attempt == 0:
                logger.log_info("Having trouble in database connection. Now trying to reconnect")
            else:
                logger.log_info("Retry attempt %s failed" % attempt)

            if attempt >= MAX_FAILED_RETRY:
                logger.log_info("Retry failed after %s attempt. Program exiting now..." % MAX_FAILED_RETRY)
                exit()

            return self.reconnect(attempt + 1)
        except pyodbc.Error as ex:
            self.connection = None
            self.connected = False
            # print(ex.args)
            sqlstate = ex.args[0]
            # print(sqlstate)
            logger.log_info("Database connection failed. Exception: %s" % str(ex))
            return self

        except Exception as exp:
            self.connection = None
            self.connected = False
            logger.log_info("Database connection failed. Exception message: %s" % str(exp))
            return self
        except KeyboardInterrupt as kexp:
            logger.log_info("Keyboard Interrupt. Program will exit now")
            exit()
            return self

    def reconnect(self, retry_count):
        rtime = randint(5, 10)

        if retry_count > 1:
            logger.log_info("Reconnect will try again in %s seconds" % rtime)
        else:
            logger.log_info("Reconnect will try in %s seconds" % rtime)

        time.sleep(rtime)

        logger.log_info("Reconnecting %s..." % retry_count)

        db_instance = self.connect(retry_count)

        if db_instance.connected:
            return self

    def __enter__(self):
        return self.connect()

    def is_connected(self):
        return self.connected

    def __exit__(self, *args):
        self.close()


if __name__ == "__main__":
    server = "cmasterdata.database.windows.net"
    port = 1433
    driver = "/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.2.so.0.1"
    user = "cm-user@cmasterdata"
    password = "23wYD8TPRhSVBggd"
    database = "cmdata"
    db_manager = DBManager(server=server, port=port, user=user, password=password, database=database, driver=driver)
    print(db_manager.connected)

    # from ddl_query_builder import DDLQueryBuilder
    # ddl_query_builder = DDLQueryBuilder()
    # table = ddl_query_builder.build_table_billing_charts_of_accounts()
    # print(table)
    # db_manager.execute(table)
    # db_manager.close()
    # print("Done!")



