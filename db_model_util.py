from ddl_query_builder import DDLQueryBuilder


class DBModelUtil(object):
    chart_of_accounts = "billingChartOfAccts"
    billing_credits = "billingCredits"
    billing_deposits = "billingDeposits"
    invoice_items = "billingInvoiceItems"
    billing_invoices = "billingInvoices"
    billing_member_fees = "billingMemberFees"
    billing_payments = "billingPayments"
    billing_receipt_items = "billingReceiptItems"
    billing_receipts = "billingReceipts"
    billing_txn_map = "billingTxnMap"
    billing_write_offs = "billingWriteoffs"
    event_registrations = "eventRegistrations"
    event_registrations_attendees = "eventRegistrationsAttendees"
    event_registrations_fees = "eventRegistrationsFees"
    events = "events"
    groups = "groups"
    hot_deals = "hotDeals"
    jobs = "jobs"
    member_billing = "memberBilling"
    member_categories = "memberCategories"
    member_custom = "memberCustom"
    member_logins = "memberLogins"
    members = "members"
    rep_groups = "repGroups"
    reps = "reps"

    batch = "batch"
    upload_status = "uploadStatus"
    upload_fail_log = "uploadFailLog"

    @classmethod
    def get_tables(cls):
        table_names = dir(cls)
        table_names = [getattr(cls, name) for name in table_names if not name.startswith('__')]
        table_names = [attr for attr in table_names if type(attr).__name__ == 'str']
        return table_names

    @classmethod
    def get_table_with_name(cls, table_name):
        tables = cls.get_tables()
        tables = [table for table in tables if table_name in table]
        return tables[0] if tables else None

    @classmethod
    def get_batch_table_name(cls):
        return cls.get_table_with_name(cls.batch)

    @classmethod
    def get_upload_status_table_name(cls):
        return cls.get_table_with_name(cls.upload_status)

    @classmethod
    def get_upload_failed_logs_table_name(cls):
        return cls.get_table_with_name(cls.upload_fail_log)

    @classmethod
    def find_best_match(cls, file_name, table_names):
        max_match_length = 0
        table_name = None

        def find_match_length(str1, str2, str_length):
            length = 0
            tstr1 = str1[:str_length]
            tstr2 = str2[:str_length]

            for ch1, ch2 in zip(list(tstr1), list(tstr2)):
                if ch1 == ch2:
                    length += 1
                else:
                    break
            return length

        cleaned_file_name = file_name
        if cleaned_file_name.startswith("cmbackup_"):
            cleaned_file_name = cleaned_file_name.replace("cmbackup_", "")

        for tname in table_names:
            min_length = min(len(tname), len(cleaned_file_name))
            match_length = find_match_length(tname, cleaned_file_name, min_length)
            if match_length > max_match_length:
                max_match_length = match_length
                table_name = tname
        return table_name

    @classmethod
    def detect_table_name(cls, file_name):
        table_names = cls.get_tables()
        table_name = cls.find_best_match(file_name, table_names)
        return table_name

    @classmethod
    def get_insert_table_columns(cls, table_name, exclude_columns=[]):
        table_columns = []
        table_fields_columns_tuple = cls.get_field_type_for_table(table_name)
        for tpl in table_fields_columns_tuple:
            table_columns += [tpl[0]]
        table_columns = tuple([tc for tc in table_columns if tc not in exclude_columns])
        return tuple(table_columns)

    @classmethod
    def get_schema_dict(cls):
        schema_dict = {
            cls.batch: DDLQueryBuilder.build_schema_table_batch(),
            cls.upload_status: DDLQueryBuilder.build_schema_table_upload_status(),
            cls.chart_of_accounts: DDLQueryBuilder.build_schema_table_billing_chart_of_accounts(),
            cls.billing_credits: DDLQueryBuilder.build_schema_table_billing_credits(),
            cls.billing_deposits: DDLQueryBuilder.build_schema_table_billing_deposits(),
            cls.invoice_items: DDLQueryBuilder.build_schema_table_billing_invoice_items(),
            cls.billing_invoices: DDLQueryBuilder.build_schema_table_billing_invoices(),
            cls.billing_member_fees: DDLQueryBuilder.build_schema_table_billing_member_fees(),
            cls.billing_payments: DDLQueryBuilder.build_schema_table_billing_payments(),
            cls.billing_receipt_items: DDLQueryBuilder.build_schema_table_billing_receipt_items(),
            cls.billing_receipts: DDLQueryBuilder.build_schema_table_billing_receipts(),
            cls.billing_txn_map: DDLQueryBuilder.build_schema_table_billing_txn_map(),
            cls.billing_write_offs: DDLQueryBuilder.build_schema_table_billing_write_offs(),
            cls.event_registrations: DDLQueryBuilder.build_schema_table_event_registrations(),
            cls.event_registrations_attendees: DDLQueryBuilder.build_schema_table_event_registrations_attendees(),
            cls.event_registrations_fees: DDLQueryBuilder.build_schema_table_event_registrations_fees(),
            cls.events: DDLQueryBuilder.build_schema_table_events(),
            cls.groups: DDLQueryBuilder.build_schema_table_groups(),
            cls.hot_deals: DDLQueryBuilder.build_schema_table_hot_deals(),
            cls.jobs: DDLQueryBuilder.build_schema_table_jobs(),
            cls.member_billing: DDLQueryBuilder.build_schema_table_member_billing(),
            cls.member_categories: DDLQueryBuilder.build_schema_table_member_categories(),
            cls.member_custom: DDLQueryBuilder.build_schema_table_member_custom(),
            cls.member_logins: DDLQueryBuilder.build_schema_table_member_logins(),
            cls.members: DDLQueryBuilder.build_schema_table_members(),
            cls.rep_groups: DDLQueryBuilder.build_schema_table_rep_groups(),
            cls.reps: DDLQueryBuilder.build_schema_table_reps(),
            cls.upload_fail_log: DDLQueryBuilder.build_schema_table_upload_fail_log()
        }
        return schema_dict

    @classmethod
    def get_fields_meta(cls, table_name):
        schema_dict = cls.get_schema_dict()

        schema = schema_dict.get(table_name)
        schema = schema[schema.find('(') + 1:]
        schema = schema[:schema.rfind(')')]
        schema_split = schema.split('\n')
        schema_split = [l.strip() for l in schema_split if l]

        field_type_name = lambda x: 'text' if 'varchar' in x else str(x)

        exclude_fields = ['id', 'date_created', 'last_updated']

        fields = []
        for schema_field in schema_split:
            if schema_field:
                field_split = schema_field.split()
                if field_split[0] not in exclude_fields:
                    fields += [(field_split[0], field_type_name(field_split[1]))]
        fields_tuple = tuple(fields)
        return fields_tuple

    @classmethod
    def get_field_type_for_table(cls, table_name):
        return cls.get_fields_meta(table_name)

    @classmethod
    def prepare_create_table_schema(cls, table_name, force_create=False):
        table_names = dir(cls)
        table_names = [getattr(cls, name) for name in table_names if not name.startswith('__')]
        if table_name not in table_names:
            return None
        schema_dict = cls.get_schema_dict()
        return schema_dict.get(table_name)

    @classmethod
    def prepare_drop_table_statement(cls, table_name, safe_drop=False):
        # table_names = dir(cls)
        # table_names = [getattr(cls, name) for name in table_names if not name.startswith('__')]
        # if table_name not in table_names:
        #     return None
        return "DROP TABLE %s" % table_name


if __name__ == "__main__":
    # print(DBModelUtil.detect_table_name("cmbackup_billingChartOfAccts.csv"))
    # attrs = DBModelUtil.get_tables()
    # for attr in attrs:
    #     print(type(attr))

    table_name = DBModelUtil.detect_table_name("cmbackup_eventRegistrations.csv")
    print(table_name)
    table_name = DBModelUtil.detect_table_name("cmbackup_eventRegistrationsFees.csv")
    print(table_name)
    table_name = DBModelUtil.detect_table_name("cmbackup_eventRegistrationsAttendees.csv")
    print(table_name)

