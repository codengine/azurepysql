class DDLQueryBuilder(object):

    @classmethod
    def build_schema_drop_table(cls, table_name):
        drop_table_query = """
            DROP TABLE %s
        """ % (table_name, )
        return drop_table_query

    @classmethod
    def build_schema_table_batch(cls):
        table_name = "batch"
        create_table_schema = """
                    CREATE TABLE %s
                    (
                      id int IDENTITY(1,1) PRIMARY KEY,
                      batch_no varchar(2000) NOT NULL,
                      batch_start_time datetime NULL,
                      batch_complete_time datetime NULL,
                      date_created datetime default CURRENT_TIMESTAMP,
                      last_updated datetime default CURRENT_TIMESTAMP
                    )
                """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_batch(cls):
        table_name = "batch"
        return cls.build_schema_drop_table(table_name)

    @classmethod
    def build_schema_table_upload_fail_log(cls):
        table_name = "uploadFailLog"
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  file_name varchar(500) NOT NULL,
                  table_name varchar(100) NOT NULL,
                  upload_status varchar(20) NOT NULL,
                  batch_no varchar(2000) NULL,
                  data varchar(MAX) NULL,
                  message varchar(MAX) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    def build_schema_drop_table_upload_fail_log(cls):
        table_name = "uploadFailLog"
        return cls.build_schema_drop_table(table_name)

    @classmethod
    def build_schema_table_upload_status(cls):
        table_name = "uploadStatus"
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  table_name varchar(500) NOT NULL,
                  csv_file_name varchar(1000) NULL,
                  upload_status varchar(20) NOT NULL, 
                  reason varchar(MAX) NULL,
                  batch_no varchar(2000) NULL,
                  rows_total_csv int NULL,
                  rows_uploaded int NULL,
                  rows_failed int NULL,
                  upload_start_time datetime default CURRENT_TIMESTAMP,
                  upload_complete_time datetime NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name, )
        return create_table_schema

    @classmethod
    def build_schema_drop_table_upload_status(cls):
        table_name = "uploadStatus"
        return cls.build_schema_drop_table(table_name)

    @classmethod
    def build_schema_table_billing_chart_of_accounts(cls, table_name = "billingChartOfAccts", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    acc_id int NULL,
                    name varchar(500) NULL,
                    type int NULL,
                    description varchar(MAX) NULL,
                    cc_id int NULL,
                    is_active varchar(5) NULL,
                    is_un_deposited_fund varchar(5) NULL,
                    account_code_id int NULL,
                    date_created datetime default CURRENT_TIMESTAMP,
                    last_updated datetime default CURRENT_TIMESTAMP 
                )
            """ % (table_name, )
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_chart_of_accounts(cls):
        return cls.build_schema_drop_table("billingChartOfAccts")

    @classmethod
    def build_schema_table_billing_credits(cls, table_name="billingCredits", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  credit_id int NULL,
                  cc_id int NULL,
                  mem_id int NULL,
                  create_date datetime NULL,
                  txn_date datetime NULL,
                  amount float NULL,
                  ref_number int NULL,
                  po_number varchar(200) NULL,
                  account_ar int NULL,
                  class_id int NULL,
                  bill_to varchar(20) NULL,
                  deliver_email varchar(5) NULL,
                  deliver_print varchar(5) NULL,
                  customer_message_id int NULL,
                  customer_message varchar(MAX) NULL,
                  memo varchar(1000) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name, )
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_credits(cls):
        return cls.build_schema_drop_table("billingCredits")

    @classmethod
    def build_schema_table_billing_deposits(cls, table_name="billingDeposits", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  deposit_id int NULL,
                  cc_id int NULL,
                  created_date datetime NULL,
                  txn_date datetime NULL,
                  account_dep int NULL,
                  memo varchar(1000) NULL,
                  cash_back float NULL,
                  cash_back_acct int NULL,
                  cash_back_memo varchar(1000),
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name, )
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_deposits(cls):
        return cls.build_schema_drop_table("billingDeposits")

    @classmethod
    def build_schema_table_billing_invoice_items(cls, table_name="billingInvoiceItems", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  invoice_item_id int NULL,
                  invoice_id int NULL,
                  cc_id int NULL,
                  fee_item_id int NULL,
                  quantity int NULL,
                  rate float NULL,
                  tax_amount float NULL,
                  account_ar varchar(500) NULL,
                  description varchar(MAX) NULL,
                  class_id varchar(500) NULL,
                  tax_set int NULL,
                  recurring_item_id int NULL,
                  initial_tax_amount float NULL,
                  frequency float NULL,
                  display_invoice_item_id int NULL,
                  account_inc int NULL,
                  event_reg_fee_id int NULL,
                  parent_item_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % table_name
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_invoice_items(cls):
        return cls.build_schema_drop_table("billingInvoiceItems")

    @classmethod
    def build_schema_table_billing_invoices(cls, table_name="billingInvoices", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  invoice_id int NULL,
                  cc_id int NULL,
                  batch_id int NULL,
                  owner_id int NULL,
                  mem_id int NULL,
                  group_id int NULL,
                  event_id int NULL,
                  created_date datetime NULL,
                  txt_date datetime NULL,
                  due_date datetime NULL,
                  amount float NULL,
                  ref_number varchar(1000) NULL,
                  po_number varchar(1000) NULL,
                  payment_term_id int NULL,
                  account_ar int NULL,
                  discount_id int NULL,
                  class_id int NULL,
                  bill_to varchar(20) NULL,
                  deliver_email varchar(5) NULL,
                  deliver_print varchar(5) NULL,
                  customer_message_id int NULL,
                  customer_message varchar(MAX) NULL,
                  memo varchar(1000) NULL,
                  email_id int NULL,
                  print_id int NULL,
                  export_qbid varchar(100) NULL,
                  allow_online_payment varchar(5) NULL,
                  epayment_queued varchar(5) NULL,
                  checkout_in_progress varchar(5) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name, )
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_invoices(cls):
        return cls.build_schema_drop_table("billingInvoices")

    @classmethod
    def build_schema_table_billing_member_fees(cls, table_name="billingMemberFees", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  fee_item_id int NULL,
                  cc_id int NULL,
                  name varchar(500) NULL,
                  is_active varchar(5) NULL,
                  fee_item_type int NULL,
                  frequency int NULL,
                  price float NULL,
                  is_taxable varchar(5) NULL,
                  is_voluntary varchar(5) NULL,
                  account_inc int NULL,
                  description varchar(MAX) NULL,
                  sort_order varchar(200) NULL,
                  sync_date datetime NULL,
                  lookup_id int NULL,
                  edit_sequence varchar(100) NULL,
                  qbid_account varchar(200) NULL,
                  qb_fn_account varchar(200) NULL,
                  collection_basis int NULL,
                  tax_set int NULL,
                  qbid_sales_tax_class varchar(200) NULL,
                  qfn_sales_tax_class varchar(200) NULL,
                  group_name varchar(200) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_member_fees(cls):
        return cls.build_schema_drop_table("billingMemberFees")

    @classmethod
    def build_schema_table_billing_payments(cls, table_name="billingPayments", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  payment_id int NULL,
                  cc_id int NULL,
                  mem_id int NULL,
                  created_date datetime NULL,
                  txt_date datetime NULL,
                  amount float NULL,
                  pay_method_id int NULL,
                  ref_number varchar(100) NULL,
                  account_ar int NULL,
                  account_dep int NULL,
                  deposit_id int NULL,
                  bill_to varchar(20) NULL,
                  memo varchar(1000) NULL,
                  export_qbid varchar(100) NULL,
                  batch_id int NULL,
                  credit_card_purchase_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
                """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_payments(cls):
        return cls.build_schema_drop_table("billingPayments")

    @classmethod
    def build_schema_table_billing_receipt_items(cls, table_name="billingReceiptItems", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  receipt_item_id int NULL,
                  cc_id int NULL,
                  receipt_id int NULL,
                  fee_item_id int NULL,
                  quantity int NULL,
                  rate float NULL,
                  tax_amount float NULL,
                  description varchar(MAX) NULL,
                  class_id int NULL,
                  tax_set int NULL,
                  display_receipt_item_id int NULL,
                  invoice_id int NULL,
                  invoice_item_id int NULL,
                  event_reg_fee_id int NULL,
                  parent_item_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_receipt_items(cls):
        return cls.build_schema_drop_table("billingReceiptItems")

    @classmethod
    def build_schema_table_billing_receipts(cls, table_name="billingReceipts", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  receipt_id int NULL,
                  cc_id int NULL,
                  mem_id int NULL,
                  event_id int NULL,
                  created_date datetime NULL,
                  txn_date datetime NULL,
                  amount float NULL,
                  pay_method_id int NULL,
                  ref_number int NULL,
                  check_number int NULL,
                  bill_to varchar(20) NULL,
                  account_dep int NULL,
                  deposit_id int NULL,
                  class_id int NULL,
                  deliver_email varchar(5) NULL,
                  deliver_print varchar(5) NULL,
                  customer_message_id int NULL,
                  customer_message varchar(MAX) NULL,
                  memo varchar(1000) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
        """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_receipts(cls):
        return cls.build_schema_drop_table("billingReceipts")

    @classmethod
    def build_schema_table_billing_txn_map(cls, table_name="billingTxnMap", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  map_id int NULL,
                  cc_id int NULL,
                  mem_id int NULL,
                  source_txn_id int NULL,
                  source_txn_type int NULL,
                  target_txn_id int NULL,
                  target_txn_type int NULL,
                  txn_amount float NULL,
                  txn_date datetime NULL,
                  created_date datetime NULL,
                  tmp_fuzzy_match varchar(1000) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
        """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_txn_map(cls):
        return cls.build_schema_drop_table("billingTxnMap")

    @classmethod
    def build_schema_table_billing_write_offs(cls, table_name="billingWriteoffs", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  write_off_id int NULL,
                  cc_id int NULL,
                  mem_id int NULL,
                  txn_date datetime NULL,
                  account_exp int NULL,
                  class_id int NULL,
                  memo varchar(1000) NULL,
                  created_date datetime NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_billing_write_offs(cls):
        return cls.build_schema_drop_table("billingWriteoffs")

    @classmethod
    def build_schema_table_event_registrations(cls, table_name="eventRegistrations", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  reg_id int NULL,
                  event_id int NULL,
                  mem_id int NULL,
                  rep_id int NULL,
                  type int NULL,
                  payment_type int NULL,
                  credit_card_transaction_id int NULL,
                  company_name varchar(1000) NULL,
                  first_name varchar(500) NULL,
                  last_name varchar(500) NULL,
                  address1 varchar(MAX) NULL,
                  address2 varchar(MAX) NULL,
                  city varchar(100) NULL,
                  state varchar(100) NULL,
                  postal_code varchar(300) NULL,
                  country varchar(300) NULL,
                  phone varchar(50) NULL,
                  fax varchar(60) NULL,
                  email varchar(300) NULL,
                  comments varchar(MAX) NULL,
                  billing_status int NULL,
                  on_roaster varchar(5) NULL,
                  notes varchar(MAX) NULL,
                  invite_status int NULL,
                  invite_comment varchar(MAX) NULL,
                  invite_datetime datetime NULL,
                  update_datetime datetime NULL,
                  online varchar(200) NULL,
                  qb_invoice_id int NULL,
                  qb_payment_id int NULL,
                  ref_number varchar(100) NULL,
                  base_fee int NULL,
                  registration_date datetime NULL,
                  qb_customer_id int NULL,
                  qb_receipt_id int NULL,
                  in_process varchar(5) NULL,
                  title varchar(500) NULL,
                  registered_by_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_event_registrations(cls):
        return cls.build_schema_drop_table("eventRegistrations")

    @classmethod
    def build_schema_table_event_registrations_attendees(cls, table_name="eventRegistrationsAttendees", force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  attendee_id int NULL,
                  reg_id int NULL,
                  mem_id int NULL,
                  rep_id int NULL,
                  first_name varchar(500) NULL,
                  last_name varchar(500) NULL,
                  fee float NULL,
                  update_time datetime NULL,
                  address1 varchar(MAX) NULL,
                  address2 varchar(MAX) NULL,
                  city varchar(200) NULL,
                  state varchar(50) NULL,
                  country varchar(200) NULL,
                  postal_code varchar(100) NULL,
                  phone varchar(200) NULL,
                  fax varchar(200) NULL,
                  email varchar(200) NULL,
                  age varchar(20) NULL,
                  comments varchar(MAX) NULL,
                  company varchar(500) NULL,
                  attended varchar(5) NULL,
                  title varchar(500) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_event_registrations_attendees(cls):
        return cls.build_schema_drop_table("eventRegistrationsAttendees")

    @classmethod
    def build_schema_table_event_registrations_fees(cls, table_name="eventRegistrationsFees",
                                                         force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  item_id int NULL,
                  reg_id int NULL,
                  fee_id int NULL,
                  price float NULL,
                  description varchar(MAX) NULL,
                  quantity int NULL,
                  age_discount int NULL,
                  volume_discount int NULL,
                  is_base_attendee varchar(5) NULL,
                  attendee_id int NULL,
                  event_fee_id int NULL,
                  tax_amount float NULL,
                  discount_id int NULL,
                  payment_item_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_event_registrations_fees(cls):
        return cls.build_schema_drop_table("eventRegistrationsFees")

    @classmethod
    def build_schema_table_events(cls, table_name="events",
                                                    force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  event_id int NULL,
                  cc_id int NULL,
                  name varchar(500) NULL,
                  status int NULL,
                  description varchar(MAX) NULL,
                  start_date datetime NULL,
                  end_date datetime NULL,
                  notes varchar(MAX) NULL,
                  url varchar(1000) NULL,
                  url_mask varchar(1000) NULL,
                  mem_id int NULL,
                  group_id int NULL,
                  sponsor_type int NULL,
                  sponsor_name varchar(1000) NULL,
                  sponsor_email varchar(200) NULL,
                  start_time datetime NULL,
                  end_time datetime NULL,
                  show_in_calendar varchar(5) NULL,
                  reminder varchar(5) NULL,
                  base_fee float NULL,
                  location varchar(MAX) NULL,
                  admission varchar(MAX) NULL,
                  hours varchar(MAX) NULL,
                  contact varchar(MAX) NULL,
                  contact_email varchar(1000) NULL,
                  is_all_day varchar(5) NULL,
                  map_address1 varchar(MAX) NULL,
                  map_city varchar(200) NULL,
                  map_state varchar(200) NULL,
                  map_postal_code varchar(50) NULL,
                  map_service int NULL,
                  map_link varchar(500) NULL,
                  recurrence_frequency int NULL,
                  recurrence_iteration int NULL,
                  recurrence_dow int NULL,
                  recurrence_date_type int NULL,
                  recurrence_end_date datetime NULL,
                  display_flags int NULL,
                  visibility_flags int NULL,
                  created_date datetime NULL,
                  version varchar(20) NULL,
                  series_event_id int NULL,
                  series_instance_override varchar(200) NULL,
                  max_attendees int NULL,
                  mult_attendees varchar(5) NULL,
                  confirm_email_consumer varchar(5) NULL,
                  confirm_email_chamber varchar(1000) NULL,
                  reg_cutoff_date datetime NULL,
                  slug varchar(1000) NULL,
                  is_published varchar(5) NULL,
                  reminder_date datetime NULL,
                  events_page_video varchar(MAX) NULL,
                  events_page_video_url varchar(1000) NULL,
                  search_description varchar(MAX) NULL,
                  favourite_count int NULL,
                  pdh varchar(2000) NULL,
                  ceu varchar(2000) NULL,
                  goal_attendance int NULL,
                  goal_sponsors int NULL,
                  goal_total_revenue float NULL,
                  goal_sponsor_revenue float NULL,
                  reg_instructions varchar(MAX) NULL,
                  event_confirmation_message varchar(MAX) NULL,
                  show_current_attendee_name_on_public varchar(5) NULL,
                  show_current_attendee_names_for_members varchar(5) NULL,
                  show_attendees_to_other_attendees varchar(5) NULL,
                  event_timezone varchar(100) NULL,
                  enable_waiting varchar(5) NULL,
                  max_reg_attendees int NULL,
                  email_qr_code varchar(5) NULL,
                  meta_description varchar(MAX) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_events(cls):
        return cls.build_schema_drop_table("events")

    @classmethod
    def build_schema_table_groups(cls, table_name="groups",
                                                    force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  group_id int NULL,
                  group_type_id int NULL,
                  cc_id int NULL,
                  name varchar(500) NULL,
                  parent varchar(500) NULL,
                  created_date datetime NULL,
                  description varchar(MAX) NULL,
                  active varchar(5) NULL,
                  slug varchar(500) NULL,
                  member_opt_int varchar(5) NULL,
                  synch varchar(5) NULL,
                  non_member_opt_in varchar(5) NULL,
                  interest varchar(5) NULL,
                  show_on_public varchar(5) NULL,
                  mic_searchable varchar(5) NULL,
                  allow_member_to_member varchar(5) NULL,
                  show_on_community_app varchar(5) NULL,
                  member_subscribe varchar(5) NULL,
                  ignore_subscribe varchar(5) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_groups(cls):
        return cls.build_schema_drop_table("groups")

    @classmethod
    def build_schema_table_hot_deals(cls, table_name="hotDeals",
                                                    force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  item_id int NULL,
                  mem_id int NULL,
                  title varchar(1000) NULL,
                  description varchar(MAX) NULL,
                  short_description varchar(MAX) NULL,
                  publish_start datetime NULL,
                  publish_end datetime NULL,
                  item_start datetime NULL,
                  item_end datetime NULL,
                  item_valid_date_description varchar(MAX) NULL,
                  value float NULL,
                  detail_url varchar(1000) NULL,
                  offer_url varchar(1000) NULL,
                  priority_id int NULL,
                  is_approved varchar(5) NULL,
                  search_result_logo_id int NULL,
                  slug varchar(1000) NULL,
                  is_published varchar(5) NULL,
                  is_purchased varchar(5) NULL,
                  created_date datetime NULL,
                  deleted varchar(5) NULL,
                  type int NULL,
                  new_publish_end varchar(500) NULL,
                  news_letter_sent_date datetime NULL,
                  status int NULL,
                  email varchar(200) NULL,
                  phone varchar(100) NULL,
                  website varchar(1000) NULL,
                  disp_url varchar(1000) NULL,
                  disp_email varchar(200) NULL,
                  tagline varchar(2000) NULL,
                  favourite_count int NULL,
                  meta_description varchar(MAX) NULL,
                  company_name varchar(500) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_hot_deals(cls):
        return cls.build_schema_drop_table("hotDeals")

    @classmethod
    def build_schema_table_jobs(cls, table_name="jobs",
                                     force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  job_id int NULL,
                  mem_id int NULL,
                  job_catg_id int NULL,
                  status int NULL,
                  is_purchased varchar(5) NULL,
                  title varchar(1000) NULL,
                  hours int NULL,
                  description varchar(MAX) NULL,
                  experience varchar(MAX) NULL,
                  responsibilities varchar(MAX) NULL,
                  skills varchar(MAX) NULL,
                  contact_rep_id int NULL,
                  contact_bus_name varchar(500) NULL,
                  contact_rep_name varchar(500) NULL,
                  contact_street varchar(1000) NULL,
                  contact_city varchar(100) NULL,
                  contact_state varchar(50) NULL,
                  contact_zip varchar(50) NULL,
                  contact_local_phone varchar(50) NULL,
                  contact_toll_free_phone varchar(50) NULL,
                  contact_fax varchar(100) NULL,
                  contact_email varchar(200) NULL,
                  contact_url varchar(500) NULL,
                  origination_date datetime NULL,
                  reminder varchar(5) NULL,
                  company_profile varchar(MAX) NULL,
                  timestamp varchar(100) NULL,
                  in_process int NULL,
                  slug varchar(500) NULL,
                  is_published varchar(5) NULL,
                  member_logo_id int NULL,
                  real_map_job_position_id int NULL,
                  real_match_job_link varchar(1000) NULL,
                  real_match_job_company_name varchar(500) NULL,
                  real_match_job_location varchar(500) NULL,
                  contact_stree2 varchar(1000) NULL,
                  meta_description varchar(MAX) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_jobs(cls):
        return cls.build_schema_drop_table("jobs")

    @classmethod
    def build_schema_table_member_billing(cls, table_name="memberBilling",
                                force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  item_id int NULL,
                  mem_id int NULL,
                  fee_id int NULL,
                  quantity int NULL,
                  fee_override float NULL,
                  frequency int NULL,
                  description varchar(MAX) NULL,
                  is_cm_fee varchar(5) NULL,
                  qb_invoice_id int NULL,
                  paid_by_cc varchar(5) NULL,
                  renewal_month varchar(50) NULL,
                  disabled int NULL,
                  active_date datetime NULL,
                  expiry_date datetime NULL,
                  disable_ach varchar(5) NULL,
                  old_frequency int NULL,
                  profile_id int NULL,
                  rep_id int NULL,
                  sort_order int NULL,
                  bill_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_member_billing(cls):
        return cls.build_schema_drop_table("memberBilling")

    @classmethod
    def build_schema_table_member_categories(cls, table_name="memberCategories",
                                          force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  mem_id int NULL,
                  category_id int NULL,
                  name varchar(1000) NULL,
                  cc_id int NULL,
                  description varchar(MAX) NULL,
                  parent varchar(1000) NULL,
                  ql_category int NULL,
                  is_lodging varchar(5) NULL,
                  is_public varchar(5) NULL,
                  slug varchar(500) NULL,
                  poi_category_id1 int NULL,
                  poi_category_id2 int NULL,
                  is_market_category varchar(5) NULL,
                  gift_card_category_id int NULL,
                  git_card_sub_category_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_member_categories(cls):
        return cls.build_schema_drop_table("memberCategories")

    @classmethod
    def build_schema_table_member_custom(cls, table_name="memberCustom",
                                             force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  mem_id int NULL,
                  profile_status1 varchar(20) NULL,
                  sold_by varchar(200) NULL,
                  naics_code3 varchar(20) NULL,
                  total_employees4 int NULL,
                  lead_source89 varchar(200) NULL,
                  competitors90 varchar(200) NULL,
                  clients92 varchar(200) NULL,
                  company_description93 varchar(MAX) NULL,
                  revenue95 varchar(100) NULL,
                  sboy_honoree_127 varchar(200) NULL,
                  business_excellence_award128 varchar(200) NULL,
                  board_membership_referral140 varchar(200) NULL,
                  date_of_board_referral196 varchar(100) NULL,
                  bd_start_date142 varchar(50) NULL,
                  bd_close_date_estimate149 varchar(50) NULL,
                  primary_reason_for_joining198 varchar(500) NULL,
                  pacesetters_priority_suppliers201 varchar(500) NULL,
                  special_business_type206 varchar(500) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_member_custom(cls):
        return cls.build_schema_drop_table("memberCustom")

    @classmethod
    def build_schema_table_member_logins(cls, table_name="memberLogins",
                                         force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  member_id int NULL,
                  member_name varchar(1000) NULL,
                  login_name varchar(100) NULL,
                  login_password varchar(1000) NULL,
                  last_login datetime NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_member_logins(cls):
        return cls.build_schema_drop_table("memberLogins")

    @classmethod
    def build_schema_table_members(cls, table_name="members",
                                         force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  mem_id int NULL,
                  name varchar(500) NULL,
                  cc_id int NULL,
                  mem_level int NULL,
                  created_date datetime NULL,
                  deleted_date datetime NULL,
                  type int NULL,
                  web_participation int NULL,
                  file_by_name varchar(2000) NULL,
                  company_name varchar(2000) NULL,
                  phone1 varchar(50) NULL,
                  phone2 varchar(50) NULL,
                  phone_cell varchar(50) NULL,
                  phone800 varchar(50) NULL,
                  fax varchar(100) NULL,
                  fax_call_first varchar(5) NULL,
                  email varchar(1000) NULL,
                  url varchar(2000) NULL,
                  phys_address1 varchar(2000) NULL,
                  phys_address2 varchar(2000) NULL,
                  phys_address3 varchar(2000) NULL,
                  phys_address4 varchar(2000) NULL,
                  phys_city varchar(100) NULL,
                  phys_state varchar(20) NULL,
                  phys_postal_code varchar(20) NULL,
                  phys_country varchar(100) NULL,
                  mail_match_phys varchar(5) NULL,
                  mail_address1 varchar(2000) NULL,
                  mail_address2 varchar(2000) NULL,
                  mail_address3 varchar(2000) NULL,
                  mail_address4 varchar(2000) NULL,
                  mail_city varchar(100) NULL,
                  mail_state varchar(20) NULL,
                  mail_postal_code varchar(20) NULL,
                  mail_country varchar(100) NULL,
                  employees_ft int NULL,
                  employees_pt int NULL,
                  join_date datetime NULL,
                  renewal_month int NULL,
                  billing_cycle int NULL,
                  drop_date datetime NULL,
                  drop_reason int NULL,
                  drop_detail varchar(MAX) NULL,
                  sales_rep int NULL,
                  stage int NULL,
                  comment varchar(MAX) NULL,
                  is_lodging varchar(5) NULL,
                  display_flags int NULL,
                  web_page_stype int NULL,
                  map_service int NULL,
                  disp_name varchar(1000) NULL,
                  disp_match_phys varchar(5) NULL,
                  disp_address1 varchar(2000) NULL,
                  disp_address2 varchar(2000) NULL,
                  disp_address3 varchar(2000) NULL,
                  disp_address4 varchar(2000) NULL,
                  disp_city varchar(100) NULL,
                  disp_state varchar(20) NULL,
                  disp_postal_code varchar(20) NULL,
                  disp_country varchar(100) NULL,
                  disp_phone1 varchar(100) NULL,
                  disp_phone2 varchar(100) NULL,
                  disp_fax varchar(100) NULL,
                  disp_email varchar(1000) NULL,
                  disp_url varchar(2000) NULL,
                  disp_url_text varchar(2000) NULL,
                  full_description varchar(MAX) NULL,
                  search_description varchar(MAX) NULL,
                  bullets varchar(MAX) NULL,
                  location_directions varchar(MAX) NULL,
                  hours_of_operation varchar(MAX) NULL,
                  lodging_description varchar(MAX) NULL,
                  lodging_vacancy_service int NULL,
                  lodging_vacancy_url varchar(1000) NULL,
                  status_type int NULL,
                  billing_rep varchar(1000) NULL,
                  slug varchar(1000) NULL,
                  is_published varchar(5) NULL,
                  member_page_video varchar(1000) NULL,
                  member_page_video_url varchar(1000) NULL,
                  member_page_action int NULL,
                  favourite_count int NULL,
                  modified_date datetime NULL,
                  established_date datetime NULL,
                  gift_card_status int NULL,
                  gitft_card_url varchar(1000) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_members(cls):
        return cls.build_schema_drop_table("members")

    @classmethod
    def build_schema_table_rep_groups(cls, table_name="repGroups",
                                         force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  roster_id int NULL,
                  cc_id int NULL,
                  group_id int NULL,
                  contact_id int NULL,
                  contact_type int NULL,
                  disabled varchar(5) NULL,
                  role varchar(500) NULL,
                  notes varchar(1000) NULL,
                  do_not_contact varchar(5) NULL,
                  created_date datetime NULL,
                  display_position varchar(500) NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_rep_groups(cls):
        return cls.build_schema_drop_table("repGroups")

    @classmethod
    def build_schema_table_reps(cls, table_name="reps",
                                      force_create=False):
        create_table_schema = """
                CREATE TABLE %s
                (
                  id int IDENTITY(1,1) PRIMARY KEY,
                  rep_id int NULL,
                  status int NULL,
                  name varchar(500) NULL,
                  prefix varchar(100) NULL,
                  first_name varchar(100) NULL,
                  middle_name varchar(100) NULL,
                  last_name varchar(100) NULL,
                  suffix varchar(100) NULL,
                  mem_id int NULL,
                  rel_to_company varchar(100) NULL,
                  relationship varchar(100) NULL,
                  title varchar(2000) NULL,
                  greeting varchar(2000) NULL,
                  work_phone varchar(50) NULL,
                  home_phone varchar(50) NULL,
                  cell_phone varchar(50) NULL,
                  alt_phone varchar(50) NULL,
                  fax varchar(100) NULL,
                  email varchar(200) NULL,
                  address1 varchar(2000) NULL,
                  address2 varchar(2000) NULL,
                  city varchar(200) NULL,
                  state varchar(50) NULL,
                  zip varchar(50) NULL,
                  country varchar(200) NULL,
                  contact_pref varchar(200) NULL,
                  phone_pref varchar(200) NULL,
                  comments varchar(MAX) NULL,
                  primary_rep varchar(5) NULL,
                  billing_rep varchar(5) NULL,
                  no_unsolicited_mail varchar(50) NULL,
                  no_html varchar(500) NULL,
                  display_on_web varchar(50) NULL,
                  company varchar(500) NULL,
                  do_not_contact  varchar(5) NULL,
                  primary_contact_for_new_member_app  varchar(5) NULL,
                  personal_bio  varchar(MAX) NULL,
                  alternative_billing_cc_email  varchar(200) NULL,
                  alternative_general_cc_email  varchar(200) NULL,
                  no_contact  varchar(5) NULL,
                  profile_id int NULL,
                  date_created datetime default CURRENT_TIMESTAMP,
                  last_updated datetime default CURRENT_TIMESTAMP
                )
            """ % (table_name,)
        return create_table_schema

    @classmethod
    def build_schema_drop_table_reps(cls):
        return cls.build_schema_drop_table("reps")

    @classmethod
    def ddl_schema_validator(cls):
        events = [
            ["EventID", "CCID", "Name", "Status", "Description", "StartDate", "EndDate", "Notes", "URL", "URLMask",
             "MemID", "GroupID", "SponsorType", "SponsorName", "SponsorEmail", "StartTime", "EndTime", "ShowInCalendar",
             "Reminder", "BaseFee", "Location", "Admission", "Hours", "Contact", "ContactEmail", "IsAllDay", "MapAddr1",
             "MapCity", "MapState", "MapPostalCode", "MapService", "MapLink", "RecurrenceFrequency",
             "RecurrenceIteration", "RecurrenceDOW", "RecurrenceDateType", "RecurrenceEndDate", "DisplayFlags",
             "VisibilityFlags", "CreatedDate", "Version", "SeriesEventID", "SeriesInstanceOverride", "MaxAttendees",
             "MultAttendees", "ConfirmEmail_Consumer", "ConfirmEmail_Chamber", "RegCutoffDate", "slug", "isPublished",
             "ReminderDate", "EventsPageVideo", "EventsPageVideoUrl", "SearchDescription", "FavoriteCount", "Pdh",
             "Ceu", "GoalAttendance", "GoalSponsors", "GoalTotalRevenue", "GoalSponsorRevenue", "RegInstructions",
             "EventConfirmationMessage", "ShowCurrentAttendeeNameOnPublic", "ShowCurrentAttendeeNamesForMembers",
             "ShowAttendeesToOtherAttendees", "EventTimeZone", "EnableWaiting", "MaxRegAttendees", "EMailQRCode",
             "MetaDescription"],
            ["2", "2779", "2001 Annual Meeting & Dinner", "2", "2001 Annual Meeting &amp; Dinner",
             "2001-05-16 17:00:00.0", "2001-05-16 00:00:00.0", "", "", "", "", "0", "", "", "", "17:00", "21:30 AM",
             "false", "false", "0.0", "", "", "", "", "", "false", "", "", "", "", "0", "", "0", "1", "0", "0", "", "0",
             "0", "2016-10-13 08:47:00.0", "2.0", "", "false", "1100", "true", "false", "", "",
             "2001-annual-meeting-dinner-05-16-2001", "false", "", "", "", "", "0", "", "", "0", "", "0.0", "", "", "",
             "false", "false", "false", "America/New_York", "false", "0", "false", ""]]

        # for tpl in zip(*events):
        #     print(tpl)

        groups = [
            ["GroupID", "GroupTypeID", "CCID", "Name", "Parent", "CreatedDate", "Description", "Active", "slug",
             "MemberOptIn", "Synch", "NonMemberOptIn", "Interest", "ShowOnPublic", "MICSearcheable",
             "AllowMemberToMember", "ShowOnCommunityApp", "MemberSubscribe", "IgnoreSubscribe"],
            ["52", "34", "2779", "Chamber Networking Group", "", "", "Chamber Networking Group", "true",
             "chamber-networking-group", "false", "true", "false", "false", "false", "false", "false", "", "false",
             "false"]
        ]

        # for tpl in zip(*groups):
        #     print(tpl)

        hot_deals = [
            ["itemId", "memId", "title", "description", "shortDesc", "publishStart", "publishEnd", "itemStart",
             "itemEnd", "itemValidDateDesc", "value", "detailUrl", "offerUrl", "priorityId", "isApproved",
             "searchResultLogoId", "slug", "isPublished", "isPurchased", "createdDate", "deleted", "type",
             "newPublishEnd", "NewsletterSentDate", "status", "email", "phone", "webSite", "dispURL", "dispEmail",
             "tagline", "FavoriteCount", "MetaDescription", "CompanyName"],
            ["2", "180035", "Discount on Diversity@Workplace Consulting Group WORKSHOPS",
             "Discounts on <a href=""https://diversityatworkplaceworkshopmay2017.eventbrite.com"">Diversity@Workplace Consulting Group Workshops</a>&nbsp;for Boston Chamber members.&nbsp;<br /><br />If interested in attending a workshop, please email <a href=""mailto:INFO@DiversityatWorkplace.com?subject=Boston%20Chamber%20Workshop%20Discount&amp;body=I%20am%20interested%20in%20attending%20Diversity%40Workplace%20Consulting%20Group%20Workshop.%20%20Please%20email%20me%20discount%20code%20for%2050%25%20off.%20"">INFO@DiversityatWorkplace.com</a> to get an exclusive discount code for 50% off.&nbsp;",
             "Discounts on Diversity@Workplace Consulting Group Workshops for Boston Chamber members. ",
             "2017-03-13 00:00:00.0", "2017-11-02 23:59:00.0", "2017-03-13 00:00:00.0", "2017-11-02 00:00:00.0", "", "",
             "", "", "1", "false", "629", "diversity-workplace-consulting-group-cambridge", "false", "true",
             "2017-03-11 07:57:00.0", "false", "2", "", "", "2", "info@diversityatworkplace.com", "(857) 209-5077",
             "http://www.diversityatworkplace.com", "http://www.diversityatworkplace.com",
             "info@diversityatworkplace.com", "workshop, diversity, consulting, business, management, inclusion", "0",
             "", "Diversity@Workplace Consulting Group"],
            ["6", "180384", "CITY WINERY is coming to Boston!",
             "<div style=""text-align: center;""><span style=""font-size:24px""><span style=""font-family:verdana""><span style=""color:#A52A2A""><span style=""font-family:arial,helvetica,sans-serif""><strong>CITY WINERY</strong></span></span> <span style=""font-family:arial,helvetica,sans-serif"">is coming to Boston!<br /><span style=""font-size:20px"">Book your PRIVATE EVENT today and receive pre-opening discounts<br />and complimentary upgrades!</span><br /><br /><span style=""font-size:18px"">Contact Krysten Mallett at krysten@citywinery.com or call (508)612-0113</span><br /><span style=""font-size:18px"">1 Canal St, Boston, MA 02114</span></span></span></span></div>",
             "Book your PRIVATE EVENT today and receive pre-opening discounts and complimentary upgrades!",
             "2017-06-12 00:00:00.0", "2017-06-17 23:59:00.0", "2017-06-12 00:00:00.0", "2017-08-31 00:00:00.0", "", "",
             "", "", "1", "false", "824", "city-winery-boston", "false", "true", "2017-06-12 13:41:00.0", "false", "2",
             "", "", "2", "krysten@citywinery.com", "(508) 612-0113", "http://www.citywinery.com",
             "http://www.citywinery.com", "", "Pre-Opening Discounts on Private Events!", "0", "", "City Winery"]
        ]

        # for tpl in zip(*hot_deals):
        #     print(tpl)

        jobs = [
            ["JobID", "MemID", "JobCatgID", "Status", "IsPurchased", "Title", "Hours", "Description", "Experience",
             "Responsibilities", "Skills", "ContactRepID", "ContactBusName", "ContactRepName", "ContactStreet",
             "ContactCity", "ContactState", "ContactZip", "ContactLocalPhone", "ContactTollFreePhone", "ContactFax",
             "ContactEmail", "ContactURL", "OriginationDate", "Reminder", "CompanyProfile", "timestamp", "InProcess",
             "slug", "isPublished", "MemberLogoId", "RealMatchJobPositionID", "RealMatchJobLink",
             "RealMatchJobCompanyName", "RealMatchJobLocation", "ContactStreet2", "MetaDescription"],
            ["1", "7567", "33", "2", "true", "Practice Innovation Manager", "", """<strong><span style=""font-family:times new roman,serif"">Practice Innovation Manager </span></strong><br />
        <strong><span style=""font-family:times new roman,serif"">Reports To:&nbsp; Director of Information Systems</span></strong><br />
        <strong><span style=""font-family:times new roman,serif"">Position Location: MetroWest</span></strong><br />
        <strong><span style=""font-family:times new roman,serif"">Responsibilities Include:</span></strong>

        <ul>
        	<li><span style=""font-family:times new roman,serif"">In collaboration with the Senior Management Team, work with Practice Area Leaders and others to develop a Knowledge Management system.</span></li>
        	<li><span style=""font-family:times new roman,serif"">In collaboration with the Senior Management Team, work with Practice Area Leaders and others to drive process improvement utilizing the Firm&#39;s existing technology, identifying software packages/platforms to meet these needs, and evaluating alternative outsourcing options.</span></li>
        	<li><span style=""font-family:times new roman,serif"">In collaboration with the Senior Management Team, work with Practice Area Leaders and others to assess current strategies and develop new ones to enhance the delivery of client service and introduce innovative efficiency tools and resources</span></li>
        	<li><span style=""font-family:times new roman,serif"">Building relationships and fostering collaborative approach to working with internal clients across practice groups and departments.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Driving and guiding the rollout and implementation of process improvements at both the practice area and firm-wide level, including developing timelines and training plans.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Helping to develop and implement project management tools that will aid attorneys and staff in the management of client matters.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Communicating application problems and issues to IT department colleagues.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Staying up-to-date with developments in both commercial and custom-built desktop applications used by the firm while keeping abreast of trends in law firms and the legal industry.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Training, coaching, and mentoring attorneys and staff on process improvement and project management.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Organizing and conducting user surveys, analyzing and reporting on the data.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Establishing metrics for measuring and reporting success and performance of process improvement measures.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Maintaining expertise on change management and user adoption as they apply to law firms.</span></li>
        	<li><span style=""color:black""><span style=""font-family:times new roman,serif"">Attending and participating in professional group meetings, maintaining awareness of new trends and developments in the industry, and makes recommendations on future practices and software needs to senior staff.</span></span></li>
        	<li><span style=""color:black""><span style=""font-family:times new roman,serif"">Attending and participating in Legal Technology Association meetings, seminars, and conferences.</span></span></li>
        </ul>
        <strong><span style=""font-family:times new roman,serif"">Required Experience, Job Related Competencies, and Essential Knowledge:</span></strong>

        <ul>
        	<li><span style=""font-family:times new roman,serif"">Experience in project management and/or professional service operations and in managing organizational change efforts. Excellent analytical and critical thinking skills.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Ability to manage multiple priorities, work cross-functionally, and meet tight deadlines.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Ability to influence others and move them toward a common vision.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Understanding of professional services organizational issues and challenges.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Excellent written and oral communication skills, including instructional and presentation skills, as well as strong interpersonal skills, with a focus on motivational skills and positive attitudes.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Ability to present information and communicate (using business-friendly and user-friendly language) calmly, with courtesy and tact, in one-on-one and small group situations, with co-workers, attorneys, supervisors, clients, and others.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Ability to absorb new ideas and concepts quickly, good problem-solving abilities, and the ability to research course development and delivery concepts.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Very strong customer service orientation, good understanding of the organization&#39;s goals and objectives, and ability to effectively prioritize and execute tasks in a high-pressure, fast-paced environment.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Experience with staff development and collaboration with human resources management, contract negotiation and vendor management.</span></li>
        	<li><span style=""font-family:times new roman,serif"">Exceptional proficiency in Microsoft Office Professional Suite (2010) and Windows Desktop OS is required.</span></li>
        </ul>
        <span style=""font-family:times new roman,serif"">This is an excellent opportunity that provides a competitive compensation package and a professional environment.&nbsp; The Practice Innovation Manager will play a key role in helping to develop and drive&nbsp; processes and systems that will be essential in helping the Firm build for the future.&nbsp; </span><br />
        <span style=""font-family:times new roman,serif"">Dedicated to Diversity:&nbsp; Bowditch &amp; Dewey focuses on inclusive hiring of those candidates having excellent skills with a broad range of experiences, knowledge and perspectives.&nbsp; Diversity within the Firm promotes an open and mutually respectful workplace and the delivery of superior service to its clients. </span><br />
        &nbsp;<br />
        <span style=""font-family:times new roman,serif"">Please submit cover letter and resume to:</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Human Resources Department</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bowditch &amp; Dewey, LLP</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 311 Main Street</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P.O. Box 15156</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Worcester, MA&nbsp; 01615-0156</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax: (508) 929-3164</span><br />
        <span style=""font-family:times new roman,serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E-mail: Jobs@bowditch.com&nbsp; </span><br />
        &nbsp;<br />
        <span style=""font-family:times new roman,serif"">Equal Opportunity Employer</span>""", "", "", "", "",
             "Bowditch & Dewey, LLP", "Vickie Manning", "One International Place, 44th Flr.", "Boston", "MA",
             "02110-9320", "(617) 757-6500", "", "", "vmanning@bowditch.com", "http://www.bowditch.com",
             "2017-03-07 09:26:00.0", "false", "", "[B@65d78be2", "0",
             "information-technology-practice-innovation-manager", "false", "", "", "", "", "", "", ""]
        ]

        # for tpl in zip(*jobs):
        #     print(tpl)

        member_billing = [
            ["ItemID", "MemID", "FeeID", "Quantity", "FeeOverride", "Frequency", "Description", "IsCMFee",
             "QBInvoiceID", "PaidByCC", "RenewalMonth", "Disabled", "ActiveDate", "ExpiryDate", "DisableACH",
             "OldFrequency", "ProfileID", "repid", "SortOrder", "billid"],
            ["1", "1023", "10", "1", "10000.0", "1", "Annual Membership Dues", "false", "", "false", "", "0",
             "2020-05-01 00:00:00.0", "", "true", "", "", "0", "1", "8"]
        ]

        # for tpl in zip(*member_billing):
        #     print(tpl)

        member_categories = [
            ["MemID", "CatgID", "Name", "CCID", "Description", "Parent", "QLCatg", "isLodging", "IsPublic", "slug",
             "POICatgID1", "POICatgID2", "isMarketCatg", "GiftCardCatgId", "GiftCardSubCatgId"],
            ["1023", "3", "Telecommunications Equipment & Services", "2779", "", "", "6", "false", "true",
             "telecommunications-equipment-services", "0", "0", "true", "", ""]
        ]

        # for tpl in zip(*member_categories):
        #     print(tpl)

        member_custom = [
            ["MemID", "Profile Status (1)", "Sold By (2)", "NAICS Code (3)", "Total Employees (4)", "Lead Source (89)",
             "Competitors (90)", "Clients (92)", "Company Description (93)", "Revenue (95)", "SBOY Honoree (127)",
             "Business Excellence Award (128)", "Board Membership Referral (140)", "Date of Board Referral (196)",
             "BD Start Date (142)", "BD Close Date Estimate (149)", "Primary Reason for Joining (198)",
             "Pacesetters Priority Suppliers (201)", "Special Business Type (206)"],
            ["2", "5", "Lilly Haddad", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
            ["1129", "High Risk", "Maura McCarthy", "541611", "1000", "0", "", "",
             "Arbella was founded in 1988 as a Massachusetts-only auto and homeowners insurance company. Consistently adding to our product lines and expanding into Rhode Island and Connecticut, Arbella has become a group of companies committed to delivering highest quality insurance products and services.",
             "", "", "", "", "", "", "", "", "", ""]
        ]

        # for tpl in zip(*member_custom):
        #     print(tpl)

        member_logins = [
            ["MemID", "MemberName", "LoginName", "LoginPwd", "LastLogIn"],
            ["180208", "Steph Stevens Photo", "steph@stephstevensphoto.com",
             "1000:uX2l1zswnxP9riL75RFI0hdwYu+WUggY:572qbbhlg5fa4nkBFIjby5MNaybaO5nX", "2018-08-03 15:57:00.0"]
        ]
        # for tpl in zip(*member_logins):
        #     print(tpl)

        members = [
            ["MemID", "Name", "CCID", "MemLevel", "CreatedDate", "DeletedDate", "Type", "WebParticipation",
             "FileByName", "CompanyName", "Phone1", "Phone2", "PhoneCell", "Phone800", "Fax", "FaxCallFirst", "Email",
             "URL", "PhysAddr1", "PhysAddr2", "PhysAddr3", "PhysAddr4", "PhysCity", "PhysState", "PhysPostalCode",
             "PhysCountry", "MailMatchPhys", "MailAddr1", "MailAddr2", "MailAddr3", "MailAddr4", "MailCity",
             "MailState", "MailPostalCode", "MailCountry", "EmployeesFT", "EmployeesPT", "JoinDate", "RenewalMonth",
             "BillingCycle", "DropDate", "DropReason", "DropDetail", "SalesRep", "Stage", "Comment", "IsLodging",
             "DisplayFlags", "WebPageStyle", "MapService", "DispName", "DispMatchPhys", "DispAddr1", "DispAddr2",
             "DispAddr3", "DispAddr4", "DispCity", "DispState", "DispPostalCode", "DispCountry", "DispPhone1",
             "DispPhone2", "DispFax", "DispEmail", "DispURL", "DispURLText", "FullDescription", "SearchDescription",
             "Bullets", "LocationDirections", "HoursOfOperation", "LodgingDescription", "LodgingVacancyService",
             "LodgingVacancyURL", "StatusType", "BillingRep", "slug", "isPublished", "MemberPageVideo",
             "MemberPageVideoUrl", "MemberPageAction", "FavoriteCount", "ModifiedDate", "EstablishedDate",
             "GiftCardStatus", "GiftCardUrl"],
            ["-1", "Greater Boston Chamber of Commerce", "2779", "10", "2017-11-01 18:43:00.0", "", "1", "10",
             "Greater Boston Chamber of Commerce", "Greater Boston Chamber of Commerce", "(617) 227-4500", "", "", "",
             "(617) 227-7505", "false", "memberservices@bostonchamber.com", "https://www.bostonchamber.com",
             "265 Franklin St., 17th Flr.", "", "", "", "Boston", "MA", "02110", "", "true",
             "265 Franklin St., 17th Flr.", "", "", "", "Boston", "MA", "02110", "", "0", "0", "", "-1", "1", "", "0",
             "", "0", "1", "", "false", "0", "0", "4", "Beth Israel Deaconess Medical Center", "true", "", "", "", "",
             "", "", "", "", "6174807695", "", "", "memerson@bidmc.harvard.edu", "", "", "", "", "", "", "", "", "1",
             "", "0", "", "greater-boston-chamber-of-commerce-boston-265-franklin-st-12th-flr", "false", "", "", "0",
             "0", "2018-11-06 12:25:01.063", "", "0", ""]
        ]

        # for tpl in zip(*members):
        #     print(tpl)

        rep_groups = [
            ["RosterID", "CCID", "GroupID", "ContactID", "ContactType", "Disabled", "Role", "Notes", "DoNotContact",
             "CreatedDate", "DisplayPosition"],
            ["25", "2779", "67", "107137", "2", "true", "", "", "false", "2016-10-13 09:20:00.0", ""]
        ]
        # for tpl in zip(*rep_groups):
        #     print(tpl)

        reps = [
            ["RepID", "Status", "Name", "Prefix", "FirstName", "MiddleName", "LastName", "Suffix", "MemID",
             "RelToCompany", "Relationship", "Title", "Greeting", "WorkPhone", "HomePhone", "CellPhone", "AltPhone",
             "Fax", "eMail", "Addr1", "Addr2", "City", "State", "Zip", "Country", "ContactPref", "PhonePref",
             "Comments", "PrimaryRep", "BillingRep", "NoUnsolicitedMail", "NoHTML", "DisplaysOnWeb", "Company",
             "DoNotContact", "PrimaryContactForNewMemberApp", "PersonalBio", "AlternativeBillingCCEmail",
             "AlternativeGeneralCCEmail", "NoContact", "profileid"],
            ["1", "2", "System Administrator", "", "System", "", "Administrator", "", "-1", "", "",
             "***system administration account***", "", "", "", "", "", "", "", "", "", "", "", "", "", "Mail", "Work",
             "", "false", "true", "", "", "", "Company Name", "false", "false", "", "", "", "false", ""]
        ]

        # for tpl in zip(*reps):
        #     print(tpl)

        invoice_items = [
            ["InvoiceItemID","InvoiceID","CCID","FeeItemID","Quantity","Rate","TaxAmt","AccountAR","Descr","ClassID","TaxSet","RecurringItemId","InitialTaxAmt","Frequency","DisplayInvoiceItemID","AccountINC","EventRegFeeId","ParentItemId"],
            ["7","50542","2779","1","1","1000.0","0.0","","Dues","","0","","","0.0","","","0","0"]
        ]
        for tpl in zip(*invoice_items):
            print(tpl)


if __name__ == "__main__":
    # https://gist.github.com/technetbytes/2720961ae365c02a0a248cc0a19db3ec
    # DDLQueryBuilder.ddl_schema_validator()
    pass

