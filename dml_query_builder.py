from datetime import datetime
from db_model_util import DBModelUtil
from etl_enums import UploadStatus


class DataWranglerModel(object):

    def cast_as_int(value):
        try:
            value = str(value).strip()
            if value:
                return int(value)
            else:
                return "null"
        except Exception as exp:
            return "null"

    @classmethod
    def cast_as_float(cls, value):
        try:
            value = str(value).strip()
            if value:
                return float(value)
            else:
                return "null"
        except Exception as exp:
            return "null"

    @classmethod
    def clean_string(cls, value):
        if value:
            return "'" + value.replace("'", "''") + "'"
        else:
            return "null"

    @classmethod
    def cast_as_datetime(cls, value, format="%Y-%m-%d %H:%M:%S.%f"):
        try:
            if value:
                datetime.strptime(value, format)
                return "'" + value + "'"
            else:
                return "null"
        except Exception as exp:
            return "null"

    @classmethod
    def clean_field(cls, field_type, value, **kwargs):
        if field_type == 'int':
            return cls.cast_as_int(value)
        elif field_type == 'float':
            return cls.cast_as_float(value)
        elif field_type == 'text':
            return cls.clean_string(value)
        elif field_type == 'datetime':
            return cls.cast_as_datetime(value, **kwargs)
        else:
            return value

    @classmethod
    def clean(cls, table_name, data_row):
        field_types = DBModelUtil.get_field_type_for_table(table_name)
        cleaned_row = []
        for (field_name, field_type), field_value in zip(field_types, data_row):
            cleaned_value = cls.clean_field(field_type, field_value)
            cleaned_row += [str(cleaned_value)]
        # print("Len: %s" % len(cleaned_row[4]))
        cleaned_row_str = ", ".join(cleaned_row)
        # print(len(cleaned_row))
        cleaned_row_str = "(" + cleaned_row_str + ")"
        # print(cleaned_row_str)
        return cleaned_row_str

    @classmethod
    def clean_invoice_items(cls, data_row):
        return cls.clean(DBModelUtil.invoice_items, data_row)

    @classmethod
    def clean_billing_invoices(cls, data_row):
        return cls.clean(DBModelUtil.billing_invoices, data_row)

    @classmethod
    def clean_billing_member_fees(cls, data_row):
        return cls.clean(DBModelUtil.billing_member_fees, data_row)

    @classmethod
    def clean_billing_payments(cls, data_row):
        return cls.clean(DBModelUtil.billing_payments, data_row)

    @classmethod
    def clean_billing_receipt_items(cls, data_row):
        return cls.clean(DBModelUtil.billing_receipt_items, data_row)

    @classmethod
    def clean_billing_receipts(cls, data_row):
        return cls.clean(DBModelUtil.billing_receipts, data_row)

    @classmethod
    def clean_billing_txn_map(cls, data_row):
        return cls.clean(DBModelUtil.billing_txn_map, data_row)

    @classmethod
    def clean_billing_write_offs(cls, data_row):
        return cls.clean(DBModelUtil.billing_write_offs, data_row)

    @classmethod
    def clean_event_registrations(cls, data_row):
        return cls.clean(DBModelUtil.event_registrations, data_row)

    @classmethod
    def clean_event_registrations_attendees(cls, data_row):
        return cls.clean(DBModelUtil.event_registrations_attendees, data_row)

    @classmethod
    def clean_event_registrations_fees(cls, data_row):
        return cls.clean(DBModelUtil.event_registrations_fees, data_row)

    @classmethod
    def clean_events(cls, data_row):
        return cls.clean(DBModelUtil.events, data_row)

    @classmethod
    def clean_groups(cls, data_row):
        return cls.clean(DBModelUtil.groups, data_row)

    @classmethod
    def clean_hot_deals(cls, data_row):
        return cls.clean(DBModelUtil.hot_deals, data_row)

    @classmethod
    def clean_jobs(cls, data_row):
        return cls.clean(DBModelUtil.jobs, data_row)

    @classmethod
    def clean_member_billing(cls, data_row):
        return cls.clean(DBModelUtil.member_billing, data_row)


class DMLQueryBuilder(object):

    @classmethod
    def prepare_dml_insert_statement(cls, table, columns=[]):
        columns_tuple = ','.join(columns)
        return "INSERT INTO %s(%s) VALUES" % (table, columns_tuple)

    @classmethod
    def prepare_dml_insert_statement_raw(cls, table, columns=[]):
        columns_tuple = ','.join(columns)
        return "INSERT INTO %s(%s) VALUES" % (table, columns_tuple)

    @classmethod
    def prepare_delete_all_statement(cls, table):
        return "DELETE FROM %s" % table

    @classmethod
    def prepare_delete_statement(cls, table, conditions):
        delete_statement = "DELETE FROM %s" % table
        if conditions:
            condition_statement = []
            for k, v in conditions.items():
                condition_statement += [k + "=" + v]
            condition_statement = " and ".join(condition_statement)

            if condition_statement:
                delete_statement = delete_statement + " where " + condition_statement
        return delete_statement

    @classmethod
    def prepare_update_statement(cls, table, fields=[], values=[], conditions={}):
        if len(fields) != len(values):
            return None
        set_values = []
        for tmp in zip(fields, values):
            set_values += [tmp[0]+"="+tmp[1]]
        set_values = ", ".join(set_values)
        update_statement = "UPDATE %s set %s" % (table, set_values, )
        if conditions:
            condition_statement = []
            for k, v in conditions.items():
                condition_statement += [k + "=" + v]
            condition_statement = " and ".join(condition_statement)

            if condition_statement:
                update_statement = update_statement + " where " + condition_statement

        return update_statement

    @classmethod
    def clean_row(cls, db_table_name, data_row):
        return DataWranglerModel.clean(db_table_name, data_row)

    @classmethod
    def clean_rows(cls, db_table_name, data_rows=[]):
        cleaned_rows = []
        failed_rows = []
        for data_row in data_rows:
            cleaned_row = cls.clean_row(db_table_name, data_row)
            if cleaned_row:
                cleaned_rows += [cleaned_row]
            else:
                failed_rows += [data_row]
        return cleaned_rows, failed_rows

    @classmethod
    def clean_rows_raw(cls, db_table_name, data_rows=[]):
        cleaned_rows = []
        failed_rows = []
        for data_row in data_rows:
            cleaned_row = cls.clean_row(db_table_name, data_row)
            if cleaned_row:
                str_cleaned_row = ", ".join(cleaned_row)
                cleaned_rows += ["(" + str_cleaned_row + ")"]
            else:
                failed_rows += [data_row]
        return cleaned_rows, failed_rows

    @classmethod
    def prepare_batch_insert_query(cls, batch_id):
        batch_table_name = DBModelUtil.get_batch_table_name()
        insert_table_columns = DBModelUtil.get_insert_table_columns(batch_table_name)
        now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")
        insert_data = [batch_id, now_dt, None]
        cleaned_data = DataWranglerModel.clean(batch_table_name, insert_data)
        insert_table_columns_str = ", ".join(insert_table_columns)
        insert_query = "INSERT INTO %s(%s) VALUES %s" % (batch_table_name, insert_table_columns_str, cleaned_data)
        return insert_query

    @classmethod
    def prepare_batch_update_query(cls, batch_id, batch_complete_time=datetime.utcnow()):
        batch_complete_time = batch_complete_time.strftime("%Y-%m-%d %H:%M:%S.0")
        batch_table_name = DBModelUtil.get_batch_table_name()
        update_query = "UPDATE %s set batch_complete_time='%s' where batch_no='%s'" % (batch_table_name,
                                                                                   batch_complete_time,
                                                                                   batch_id)
        return update_query

    @classmethod
    def prepare_upload_status_insert_query(cls, batch_id, file_name,
                                           rows_total_csv, upload_start_time=datetime.utcnow(),
                                           upload_status=UploadStatus.STARTED.value):
        upload_status_table = DBModelUtil.get_upload_status_table_name()
        table_name = DBModelUtil.detect_table_name(file_name)
        insert_table_columns = DBModelUtil.get_insert_table_columns(upload_status_table)
        upload_start_time = upload_start_time.strftime("%Y-%m-%d %H:%M:%S.0")
        reason = None
        rows_uploaded = None
        rows_failed = None
        upload_complete_time = None
        insert_data = [table_name, file_name, upload_status, reason, batch_id, rows_total_csv,
                       rows_uploaded, rows_failed, upload_start_time, upload_complete_time]
        cleaned_data = DataWranglerModel.clean(upload_status_table, insert_data)
        insert_table_columns_str = ", ".join(insert_table_columns)
        insert_query = "INSERT INTO %s(%s) VALUES %s" % (upload_status_table, insert_table_columns_str, cleaned_data)
        return insert_query

    @classmethod
    def prepare_upload_status_update_query(cls, batch_id, file_name, upload_status, reason=None, rows_uploaded=None, rows_failed=None,
                                           upload_complete_time=None):
        if all([not reason, not rows_uploaded, not rows_failed, not upload_complete_time]):
            return None

        upload_status_table = DBModelUtil.get_upload_status_table_name()

        table_name = DBModelUtil.detect_table_name(file_name)

        set_query_list = []
        set_query_list += ["upload_status='%s'" % upload_status]
        if reason:
            set_query_list += ["reason='%s'" % reason]
        if rows_uploaded >= 0:
            set_query_list += ["rows_uploaded=%s" % rows_uploaded]
        if rows_failed >= 0:
            set_query_list += ["rows_failed=%s" % rows_failed]
        if upload_complete_time:
            upload_complete_time = upload_complete_time.strftime("%Y-%m-%d %H:%M:%S.0")
            set_query_list += ["upload_complete_time='%s'" % upload_complete_time]

        set_query_string = ", ".join(set_query_list)

        where_condition = "where batch_no='%s' and table_name='%s'" % (batch_id, table_name)

        update_querystring = "UPDATE %s set %s %s" % (upload_status_table, set_query_string, where_condition)

        return update_querystring

    @classmethod
    def prepare_failed_log_insert_query(cls, data=[]):
        if not data:
            return None

        failed_log_table = DBModelUtil.get_upload_failed_logs_table_name()
        insert_table_columns = DBModelUtil.get_insert_table_columns(failed_log_table)

        clean_data = []
        for data_row in data:
            cleaned_row = DataWranglerModel.clean(failed_log_table, data_row)
            clean_data += [cleaned_row]

        insert_table_columns_str = ", ".join(insert_table_columns)

        cleaned_data_string = ", ".join(clean_data)

        insert_query = "INSERT INTO %s(%s) VALUES %s" % (failed_log_table, insert_table_columns_str, cleaned_data_string)
        return insert_query


if __name__ == "__main__":
    # table_name = "chart_of_accounts"
    # data_row = [
    #     ['169', 'Checking  ', '111', 'Checking  ', '2779', 'true', 'false', '107'],
    #     ['170', 'Unapplied payments ', '112', 'Unapplied payments ', '2779', 'true', 'false', '108'],
    #     ['171', 'Deferred Income-Chamber Networking ', '112', 'Deferred Income-Chamber Networking ', '2779', 'true',
    #      'false', '129'],
    #     ['172', 'Deferred Income ', '112', 'Deferred Income ', '2779', 'true', 'false', '132'],
    #     ['173', "Deferred Income - Women's Network Roundtable ", '112', "Deferred Income - Women's Network Roundtable ", '2779', 'true', 'false', '189'],
    #     ['174', "Deferred Income - Women's Forum ", '112', "Deferred Income - Women's Forum ", '2779', 'true', 'false', '164'],
    #     ['175', 'Deferred Income - EOB ', '112', 'Deferred Income - EOB ', '2779', 'true', 'false', '138'],
    #     ['176', 'Deferred Income - Annual Meeting ', '112', 'Deferred Income - Annual Meeting ', '2779', 'true', 'false', '119'],
    #     ['177', 'Deferred Income - Financial Services Forum ', '112', 'Deferred Income - Financial Services Forum ', '2779', 'true', 'false', '106']
    # ]
    #
    # cleaned_row = DMLQueryBuilder.clean_rows(table_name, data_row)
    # print(cleaned_row)

    # batch_insert_query = DMLQueryBuilder.prepare_batch_insert_query("1234")
    # # print(batch_insert_query)
    # batch_update_query = DMLQueryBuilder.prepare_batch_update_query("1234")
    # print(batch_update_query)

    # upload_status_insert_query = DMLQueryBuilder.prepare_upload_status_update_query(batch_id='1234',
    #                                                                                 reason='reason_name',
    #                                                                                 rows_uploaded=100)
    # print(upload_status_insert_query)

    data = [
        ['file_name', 'table_name', 'FAILED', '1234', '{}', 'Exception message'],
        ['file_name', 'table_name', 'FAILED', '1234', '{}', 'Exception message']
    ]

    failed_log_insert_querystring = DMLQueryBuilder.prepare_failed_log_insert_query(data)
    print(failed_log_insert_querystring)