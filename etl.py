import os
import sys, getopt
import json
import random
from config_manager import ConfigManager


config = ConfigManager()

directory, file_name, file_path = config.read_lock_file_path()

with open(file_path, "w") as fp:
    value = random.getrandbits(40)
    fp.write(str(value))


from etl_context import ETLProgram
from etl_context import ETLBatchContext
from etl_logger import ETLLogger
from file_manager import FileManager
from clock import Clock
from etl_exception import ETLException
from db_manager import DBManager
from db_model_util import DBModelUtil
from test_etl import script_test
from test_etl import bulk_insert_test
from processor import handle_file_processing
from etl_downloader import ETLDownloader


logger = ETLLogger.get_logger()


class Initializer(object):
    def __init__(self):
        logger.log_info(message="Program run successfully")

    @classmethod
    def check_prerequisites(cls):
        logger.log_info("------------------------------------------------------------")
        logger.log_info("Checking if the config file exist")
        config_file_name = "config.json"
        if not os.path.exists(config_file_name):
            logger.log_info("%s file does not exist." % config_file_name)
            return False
        logger.log_info("Checking if the directory is writable")
        if not os.access(os.getcwd(), os.W_OK):
            logger.log_warning("Directory %s is not writable. Exiting..." % os.getcwd())
            return False
        return True


def create_tables(db_instance, tables=[]):

    logger.log_info("======================= Creating tables... ===========================")

    table_names = tables or DBModelUtil.get_tables()

    logger.log_info("Total %s tables found" % len(table_names))

    for table_name in table_names:
        create_table_schema = DBModelUtil.prepare_create_table_schema(table_name)

        logger.log_info("Creating table for %s" % table_name)

        if create_table_schema:
            logger.log_info("Schema detected: %s" % str(create_table_schema))

            created = db_instance.execute_ddl_query(create_table_schema)

            if created:
                logger.log_info("Database table created for %s" % table_name)
            else:
                logger.log_info("Database table create failed")

        else:
            logger.log_info("No schema definition found for %s" % table_name)

    logger.log_info("=============== End creating tables ==================================")


def drop_tables(db_instance, tables=[]):
    logger.log_info("======================= Dropping tables... ===========================")

    table_names = tables or DBModelUtil.get_tables()

    logger.log_info("Total %s tables found" % len(table_names))

    for table_name in table_names:
        drop_table_schema = DBModelUtil.prepare_drop_table_statement(table_name)

        logger.log_info("drop table for %s" % table_name)

        if drop_table_schema:
            logger.log_info("statement to be executed: %s" % str(drop_table_schema))

            dropped = db_instance.execute_ddl_query(drop_table_schema)

            if dropped:
                logger.log_info("Database table dropped for %s" % table_name)
            else:
                logger.log_info("Database table drop failed")

        else:
            logger.log_info("No statement found for %s" % table_name)

    logger.log_info("=============== End dropping tables ==================================")


def parse_command_line_arguments(argv, allowed_actions=['create-tables', 'delete-tables', 'test',
                                                        'force-reset', 'test-bulk-insert', 'run', 'run-sync']):

    option = None
    try:
        opts, args = getopt.getopt(argv, "ha:", ["action="])
    except getopt.GetoptError:
        print('etl.py -a <action-name>')
        opts = {}

    for opt, arg in opts:
        if opt == '-h':
            print('etl.py -a <action-name>')
            sys.exit()
        elif opt in ("-a", "--action"):
            option = arg
        else:
            print("The option %s is not supported" % opt)
            sys.exit()

    if option and option not in allowed_actions:
        print("Allowed actions are: %s" % str(allowed_actions))
        sys.exit()

    if not option:
        print("Allowed actions are: %s" % str(allowed_actions))
        sys.exit()

    return option


def read_all_csv_files(csv_files_directory):
    logger.log_info("Read all csv files from: %s" % csv_files_directory)

    csv_files = FileManager.read_all_csv_files(csv_files_directory)

    logger.log_info("Total %s csv files found" % len(csv_files))

    return csv_files


def configure_tracker(config, csv_files_directory, csv_files):
    logger.log_info("Initializing the tracker")

    ut_dir, tracker_file_name, tracker_file_path = config.read_upload_tracker_path()

    current_track_status = FileManager.read_tracker_file(tracker_file_path)

    logger.log_info("Checking if it is a fresh start")
    fresh_start = FileManager.check_if_fresh_start(tracker_file_path)
    if fresh_start:
        logger.log_info("Fresh start detected")

        if not os.path.exists(tracker_file_path):
            logger.log_info("Tracker file not found. Creating now...")

            FileManager.build_upload_tracker_file(tracker_file=tracker_file_path, files=csv_files)

            logger.log_info("Tracker created.")
        else:
            logger.log_info("Tracker file exists. updating the tracking status")

            FileManager.build_upload_tracker_file(tracker_file=tracker_file_path, files=csv_files,
                                                  current_status=current_track_status)
            logger.log_info("Tracker updated.")

        logger.log_info("Counting all rows for all CSV files")

        row_count = FileManager.count_file_rows(csv_files_directory)

        logger.log_info("Count done as following: ")
        logger.log_info(json.dumps(row_count))

        row_count_cfg_dir, row_count_file_name, row_count_cfg_file = config.read_row_count_file_path()
        logger.log_info("Row count config file detected: %s" % row_count_cfg_file)

        if not os.path.exists(row_count_cfg_dir):
            os.makedirs(row_count_cfg_dir)

        if os.path.exists(row_count_cfg_file):
            os.remove(row_count_cfg_file)

        logger.log_info("Saving row count in config: %s" % row_count_cfg_file)

        with open(row_count_cfg_file, "w", encoding='utf-8') as rcf:
            json.dump(row_count, rcf)

        logger.log_info("Row count saved in config file")

    else:
        logger.log_info("This is not a fresh start. Upload will resume")

    logger.log_info("Reading tracker file again to process the data and save to database")
    tracker = FileManager.read_tracker_file(tracker_file_path)
    logger.log_info("File read done")


def main(argv):

    option = parse_command_line_arguments(argv)

    logger.log_info("Program will start with option: %s from the command line" % option)

    logger.log_info("Inside the main()")
    now_time = Clock.read_current_datetime()
    logger.log_info("Program started at %s" % now_time)
    logger.log_info("Checking prerequisites...")
    pre_check_result = Initializer.check_prerequisites()

    if pre_check_result is not True:
        logger.log_info("prerequisites failed. Raising exception and exiting...")
        raise ETLException("Prerequisite Check Failed")

    logger.log_info("Prerequisite check successful. Got go ahead..")

    logger.log_info("Reading config")
    config = ConfigManager()

    csv_files_directory = config.read_csv_directory()
    if not csv_files_directory:
        logger.log_warning("CSV files directory not configured in config.json.")
        raise ETLException("CSV Files directory not configured in config.json")

    temp_directory = config.read_temp_directory() or os.path.join(os.getcwd(), 'tmp')

    logger.log_info("Temp directory configured: %s" % temp_directory)

    archive_directory = config.read_archive_directory() or os.path.join(os.getcwd(), 'Archived')

    if not os.path.exists(archive_directory):
        os.makedirs(archive_directory)

    logger.log_info("Archive directory configured: %s" % archive_directory)

    ut_dir, tracker_file_name, tracker_file_path = config.read_upload_tracker_path()

    server = config.read_server()
    port = config.read_port()
    user = config.read_user()
    password = config.read_password()
    database = config.read_database()
    driver = config.read_driver()

    db = DBManager(server=server, port=port, user=user, password=password, database=database, driver=driver,
                   turbo=False)

    if option == "run-sync":
        logger.log_info("%s command was requested. Deleting the tracker file %s and temp directory %s" % (
            option, tracker_file_path, temp_directory))

        FileManager.delete_file(tracker_file_path)
        FileManager.remove_directory(temp_directory)

        logger.log_info("tracker and temp file removed.")

        logger.log_info("Downloader initializing...")

        etl_downloader = ETLDownloader()
        with etl_downloader:
            etl_downloader.start_download_sync()

        logger.log_info("Download finished. Now uploader will take place")

        csv_files = read_all_csv_files(csv_files_directory)

        configure_tracker(config, csv_files_directory, csv_files)

        logger.log_info("Tracker Re-initializing...")

        FileManager.build_upload_tracker_file(tracker_file=tracker_file_path, files=csv_files)

        logger.log_info("Tracker re-initialized.")

        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                handle_file_processing(db_instance, csv_files_directory, temp_directory,
                                       tracker_file_path, archive_directory, config)

    elif option == "run":
        logger.log_info("%s command was requested. Deleting the tracker file %s and temp directory %s" % (
        option, tracker_file_path, temp_directory))

        FileManager.delete_file(tracker_file_path)
        FileManager.remove_directory(temp_directory)

        logger.log_info("tracker and temp file removed.")

        logger.log_info("Downloader initializing...")

        download_finished = False
        etl_downloader = ETLDownloader()
        with etl_downloader:
            download_finished = etl_downloader.start_download()

        if download_finished:
            logger.log_info("Download finished. Now uploader will take place")
        else:
            logger.log_info("Download did not finish for all tables. Uploader will take place for the downloaded files only")

        csv_files = read_all_csv_files(csv_files_directory)

        configure_tracker(config, csv_files_directory, csv_files)

        logger.log_info("Tracker Re-initializing...")

        FileManager.build_upload_tracker_file(tracker_file=tracker_file_path, files=csv_files)

        logger.log_info("Tracker re-initialized.")

        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                handle_file_processing(db_instance, csv_files_directory, temp_directory,
                                       tracker_file_path, archive_directory, config)
    elif option == "force-reset":
        logger.log_info("force delete was requested. Deleting the tracker file %s and temp directory %s" % (tracker_file_path, temp_directory))

        FileManager.delete_file(tracker_file_path)
        FileManager.remove_directory(temp_directory)

        logger.log_info("tracker and temp file removed. Now reinitializing...")

        csv_files = read_all_csv_files(csv_files_directory)

        FileManager.build_upload_tracker_file(tracker_file=tracker_file_path, files=csv_files)

        logger.log_info("Tracker re-initialized.")

        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                handle_file_processing(db_instance, csv_files_directory, temp_directory,
                                       tracker_file_path, archive_directory, config)

    elif option == "create-tables":
        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                create_tables(db_instance)

    elif option == "delete-tables":
        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                drop_tables(db_instance)

    elif option == "test":
        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                script_test(db_instance, argv)

    elif option == "test-bulk-insert":
        with db as db_instance:
            batch_context = ETLBatchContext(db_instance=db_instance, option=option)
            with batch_context as bc:
                if not db_instance.is_connected():
                    logger.log_info("Database not connected. Exiting...")
                    exit()
                bulk_insert_test(db_instance, argv)


if __name__ == "__main__":

    """
    The etl.py is the entry point to start.
    
    How to run
    ================================================================
    1. Clone the project
    2. CD to the project directory
    3. Now run etl.py as following
       - etl.py supports four basic command: start, force-reset, create-tables, delete-tables
       - You need to provide option name with --action argument. For example, 
         python3 etl.py --action=force-reset
       - The start option resumes the program
       - The force-reset forceably reset the upload
       - create-tables creates all database tables
       - delete-tables deleted all database tables
    4. At start the program reads the config.json file which is located directly inside the project directory
    
    Understanding the config.json file
    ================================================================
    1. The 'database' node contains the database connection information
    2. The 'files' node contains the configuration regarding where CSV files located. The
       'csv_directory' key in node 'files' contains the full path of the CSV files directory
    3. 'temp_directory' in 'files' node can be configured for the temp directory which will be used by the
       program to split files into smaller temp files to upload. The directory must be writtable
    4. 'archive_directory' in 'files' node can be configured for archive directory. Basically where files will
       be moved once uploaded. This directory must be writtable. For already existing files with the same name
       like previous days file, it first removes the file and then copy the new one.
    5. 'log_file' in 'log' node can be configured to log files the program write. The file must be writable.
    6. 'config' node you can ignore.
    7. Please make sure the project directory is writable by the current user.
    
    
    Application Architecture
    ================================================================
    1. The application is layout flat structure. So all files resides in the same project directory
    2. Several context class is used to wrap different portion of the program and also to make sure 
       few things must happened regardless of pre-exit or mature exit of the program
    3. The main program is wrapped up by ETLProgram context class. So the program uses a file to determine the 
       status of the program completion and it generates a batch ID which is put inside the lock file
       that is being used by throughout the program to make database transaction. It's kind of unique
       key for the whole batch run.
    4. The db_manager.py also implemented context mechanism which make sure the db closing when exiting the program
    5. The main source file is now split into 1000 size chunks and upload each temp chunk file once. The
       whole main source file upload is wrapped up using a context class which make sure to perform
       certain operation when the whole source file is uploaded. Also each temp file is wrapped by another context
       class. 
    6. The db_manager is well written to handle network interruption. When it fails to perform any transaction and
       get caught in the except clause it tries to reconnect 20 times after which it finally exit the program
    7. When one main 1000 size batch failed, it tries for smaller batch with size 100. If even it fails then it 
        tries in single row and save single rows in local json file
    8. The json file is later uploaded when the main source upload is completed.
    9. The application verifies the number of rows in csv files and the rows uploaded for each csv file. Based on
       that it puts status whether it is complete or partially complete.
    """

    etl_program = ETLProgram()
    with etl_program as etl:
        etl.run_main(main, sys.argv[1:])







