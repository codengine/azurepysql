import time
import random
import os, uuid
from datetime import datetime
from config_manager import ConfigManager
from etl_logger import ETLLogger
from file_manager import FileManager
from dml_query_builder import DMLQueryBuilder


logger = ETLLogger.get_logger()


class ETLSmallBatchUnit(object):
    def __init__(self, db_instance, parent_batch_size, batch_size):
        self.db_instance = db_instance
        self.batch_start = 0
        self.parent_batch_size = parent_batch_size
        self.batch_size = batch_size
        self.batch_finished = False

    def update_batch_point(self, batch_start):
        self.batch_start = batch_start

    def update_batch_finished(self, status):
        self.batch_finished = status

    def __enter__(self):
        logger.log_info("=========================Begin ETL Small Batch Upload======================")
        return self

    def __exit__(self, *args):
        logger.log_info("=========================End ETL Small Batch Upload========================")


class ETLFileUploadContext(object):
    def __init__(self, db_instance, csv_file,config_manager_instance):
        self.db_instance = db_instance
        self.csv_file = csv_file
        self.config_manager_instance = config_manager_instance

    def __enter__(self):
        logger.log_info("=========================Begin ETL File Upload %s===========================" % self.csv_file)
        self.start = time.time()
        return self

    def __exit__(self, *args):
        logger.log_info("Checking if any failed row found for %s" % self.csv_file)
        d, name, failed_row_path = self.config_manager_instance.read_failed_log_json_file_path()
        failed_rows = FileManager.read_failed_rows(self.csv_file, failed_row_path)
        if failed_rows:
            logger.log_info("%s failed rows found to upload" % len(failed_rows))
            logger.log_info("===========================Start Failed Rows=====================")
            logger.log_info(str(failed_rows))
            logger.log_info("===========================End Failed Rows=======================")

            upload_query = DMLQueryBuilder.prepare_failed_log_insert_query(data=failed_rows)

            execute_status, exception_message = self.db_instance.execute_dml_query(upload_query)

            if execute_status:
                logger.log_info("Failed rows uploaded successfully")

                logger.log_info("Clearing the json file")

                FileManager.clear_json_file(failed_row_path)

                logger.log_info("json files cleared")

            else:
                logger.log_info("Failed rows upload failed. Message: %s" % exception_message)

        else:
            logger.log_info("No failed rows found to upload")

        end = time.time()
        logger.log_info("Time taken to upload %s: %s seconds" % (self.csv_file, end-self.start))

        logger.log_info("============================End ETL File Upload %s========================" % self.csv_file)


class ETLBatchContext(object):

    def __init__(self, db_instance, option):
        self.db_instance = db_instance
        self.option = option

    def __enter__(self):
        if self.option in ['start', 'force-reset']:
            logger.log_info("*************************************Begin ETL Batch********************************")

            now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")

            logger.log_info("Starting ETL Batch at %s" % now_dt)

            config = ConfigManager()

            directory, file_name, file_path = config.read_lock_file_path()

            batch_id = FileManager.read_etl_batch_id(file_path)
            logger.log_info("ETL Batch ID found: %s" % batch_id)

            if not batch_id:
                logger.log_info("Batch ID not found. Exiting...")
                exit()

            batch_insert_query = DMLQueryBuilder.prepare_batch_insert_query(batch_id)

            execute_status, exception_message = self.db_instance.execute_dml_query(batch_insert_query)

            if execute_status:
                logger.log_info("Batch info inserted in SQL DB")
            else:
                logger.log_info("Batch info could not be inserted in SQL DB.\n Exiting now... \nException: %s" % exception_message)

        return self

    def __exit__(self, *args):
        if self.option in ['start', 'force-reset']:
            now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")
            logger.log_info("Finishing ETL Batch at %s" % now_dt)

            config = ConfigManager()

            directory, file_name, file_path = config.read_lock_file_path()

            batch_id = FileManager.read_etl_batch_id(file_path)
            logger.log_info("Finishing ETL Batch with id: %s" % batch_id)

            batch_update_query = DMLQueryBuilder.prepare_batch_update_query(batch_id)

            execute_status, exception_message = self.db_instance.execute_dml_query(batch_update_query)

            if execute_status:
                logger.log_info("Batch info updated in SQL DB")
            else:
                logger.log_info(
                    "Batch info could not be updated in SQL DB.\n Exiting now... \nException: %s" % exception_message)

            logger.log_info("Checking if all files processed in the upload tracker file")

            ut_dir, tracker_file_name, tracker_file_path = config.read_upload_tracker_path()

            all_processed = FileManager.check_if_all_processed_in_upload_tracker_file(tracker_file_path)

            if all_processed:
                logger.log_info("All files processed in the upload tracker file. Clearing it before the next batch run")
                FileManager.clear_upload_tracker_file(tracker_file_path)
            else:
                logger.log_info("All files not processed in the upload tracker file. So leaving it as it is.")

            logger.log_info("*************************************End ETL Batch********************************")


class ETLProgram(object):

    def run_main(self, method_name, argv):
        method_name(argv)

    def uniqueid(self):
        seed = random.getrandbits(40)
        return seed

    def __enter__(self):
        now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")

        logger.log_info("Starting ETL program at %s" % now_dt)

        config = ConfigManager()

        lock_dir, lock_fname, lock_fpath = config.read_lock_file_path()

        batch_id = ""
        with open(lock_fpath, "r") as f:
            batch_id = f.read()

        logger.log_info("*************************************Begin ETL Program - %s********************************" % batch_id)

        # config = ConfigManager()
        #
        # directory, file_name, file_path = config.read_lock_file_path()
        #
        # if os.path.exists(file_path):
        #     logger.log_info("Please ensure the previous upload finished completely. "
        #                     "Seems there is an already running program. Existing now")
        #     exit()
        #
        logger.log_info("Acquiring lock for the program")
        # with open(file_path, "w") as fp:
        #     value = self.uniqueid()
        #     fp.write(str(value))
        #
        logger.log_info("Lock acquired successfully")

        return self

    def __exit__(self, *args):
        now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")
        logger.log_info("Finishing ETL program at %s" % now_dt)

        config = ConfigManager()

        lock_dir, lock_fname, lock_fpath = config.read_lock_file_path()

        batch_id = ""
        with open(lock_fpath, "r") as f:
            batch_id = f.read()

        directory, file_name, file_path = config.read_lock_file_path()

        logger.log_info("Release the lock now")
        if not os.path.exists(file_path):
            logger.log_info("Lock not found to release")
            exit()

        os.remove(file_path)

        logger.log_info("Lock released successfully")

        logger.log_info("*************************************End ETL Program - %s********************************" % batch_id)