import requests, sys, datetime, os, urllib3
import time
import asyncio
from contextlib import closing
import aiohttp
import async_timeout
from etl_logger import ETLLogger
from config_manager import ConfigManager


logger = ETLLogger.get_logger()


class ETLDownloader(object):

    @classmethod
    def get_file_commands(cls):
        commands = [
            'deliverMembers',
            'deliverMemberLogins',
            'deliverMemberCatgs',
            'deliverMemberBilling',
            'deliverMemberCustom',
            'deliverReps',
            'deliverRepGroups',
            'deliverBillingChartOfAccts',
            'deliverBillingFeeItems',
            'deliverBillingCredits',
            'deliverBillingCreditsItems',
            'deliverBillingReceipts',
            'deliverBillingReceiptItems',
            'deliverBillingWriteoffs',
            'deliverBillingTxnMap',
            'deliverBillingDeposits',
            'deliverBillingInvoices',
            'deliverBillingInvoiceItems',
            'deliverBillingPayments',
            'deliverEvents',
            'deliverEventsRegistrations',
            'deliverEventsRegistrationsFees',
            'deliverEventsRegistrationsAttendees',
            'deliverGroups',
            'deliverHotDeals',
            'deliverJobs'
        ]
        # commands = ["deliverEventsRegistrationsAttendees"]
        return commands

    def __init__(self):
        self.config = ConfigManager()
        self.download_directory = self.config.read_download_directory()
        self.login_username, self.login_password = self.config.read_login_credentials()
        self.login_url = 'https://secure2.chambermaster.com/login/authenticate'
        self.file_commands = self.__class__.get_file_commands()
        self.download_ready = {fcommand: False for fcommand in self.__class__.get_file_commands()}
        self.download_urls = {fcommand: None for fcommand in self.__class__.get_file_commands()}
        self.downloaded = {}
        self.sem = asyncio.Semaphore(5)

    def clear_download_directory(self):
        logger.log_info('Clearing the download directory')
        download_directory = self.config.read_download_directory()

        logger.log_info('Download directory found %s' % download_directory)

        all_files = os.listdir(download_directory)

        for file in all_files:
            if file.endswith('.csv'):
                file_path = os.path.join(download_directory, file)
                os.remove(file_path)
                logger.log_info('Deleted file: %s' % file)

    def create_download_directory(self):
        try:
            if not os.path.exists(self.download_directory):
                os.makedirs(self.download_directory)
        except Exception as exp:
            pass

    def perform_login(self):
        try:
            session = requests.Session()
            auth_payload = {'UserName': self.login_username, 'Password': self.login_password}
            login = session.post(self.login_url, data=auth_payload)
            if login.status_code == 200 and 'Greater Boston Chamber of Commerce' in login.text:
                logger.log_info("Login Successful")
            else:
                logger.log_info('Login failed, aborting')
                sys.exit()
            return session
        except Exception as exp:
            logger.log_info("Login failed. Exception: %s" % str(exp))

    def __enter__(self):
        logger.log_info("***************************** Download Start *******************************")
        if self.download_directory:
            if not os.access(self.download_directory, os.W_OK):
                logger.log_info('Download directory does not have any write permission. Existing now...')
                exit()

        self.clear_download_directory()

        self.start_time = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.log_info("Ending the session")
        # self.session.close()
        end_time = time.time()
        logger.log_info("Time taken: %s seconds" % (end_time - self.start_time))
        logger.log_info("***************************** Download End **********************************")

    async def download_single_file(self, file_command, download_url, session):
        try:
            chunk_size = 1024
            download_finished = False
            download_file_path = None
            if len(download_url) > 5:
                logger.log_info('Downloading file: ' + download_url)
                local_filename = download_url.split('/')[-1]

                download_file_path = os.path.join(self.download_directory, local_filename)

                # https://gist.github.com/Hammer2900/2b5da5c08f6406ab49ddb02b0c5ae9f7
                # https://docs.aiohttp.org/en/stable/client_quickstart.html#streaming-response-content

                # with async_timeout.timeout(10000):
                async with session.get(download_url) as response:
                    message = None
                    if response.status == 200:
                        with open(download_file_path, 'wb') as fd:
                            while True:
                                chunk = await response.content.read(chunk_size)
                                if not chunk:
                                    download_finished = True
                                    break
                                fd.write(chunk)
                    else:
                        text = await response.text()
                        message = "File download could not be possible. Http Status: %s, Reason: %s" % (response.status, text)
                        logger.log_info('File download could not be possible. Status return %s and text: %s' %
                                        (response.status, text))
                    # For large files use response.content.read(chunk_size) instead.
                    return file_command, download_url, download_file_path, download_finished, message

            else:
                logger.log_info('ERROR: download URL invalid')
                return file_command, download_url, download_file_path, False, "Download URL Invalid"
        except Exception as exp:
            logger.log_info('ERROR: Unable to download file. Exception: %s' % str(exp))
            return file_command, download_url, download_file_path, False, str(exp)

    async def donwload_single_file_async(self, file_command, session):
        backup_url = 'https://secure2.chambermaster.com/directory/jsp/chamber/Backup.jsp'
        payload = {'command': file_command}
        logger.log_info("Generating backup for %s" % file_command)
        try:
            response = await session.post(backup_url, data=payload, timeout=3000)
        except requests.exceptions.ConnectionError as e:
            logger.log_info("ERROR: Connection Problem. Exception: %s" % str(e))
        except requests.exceptions.Timeout as e:
            logger.log_info("ERROR: Timeout. Exception: %s" % str(e))
        except requests.exceptions.RequestException as e:
            logger.log_info("ERROR: General Error. Exception: %s" % str(e))
        except KeyboardInterrupt:
            logger.log_info("ERROR: Program exited before completing")
            exit()

        try:
            text = await response.text()
            if response.status == 200 and 'window.open("/directory/jsp/reports/Completed.jsp?' in text:
                logger.log_info("Success")
                return_string = text
                start_position = return_string.find('file=/directory/reports/2779/', 0, 200)
                end_position = return_string.find('.csv",', start_position, start_position + 200)
                file_name = return_string[start_position + 29:end_position + 4]
                if len(file_name) > 5:
                    file_url = 'https://secure2.chambermaster.com/directory/reports/2779/' + file_name
                else:
                    logger.log_info("ERROR: " + file_command + ' file name too short to be valid; aborting')
                    logger.log_info("ERROR: file name retreived was " + file_name)
                    file_url = ''
            elif 'No records were found.' in text:
                logger.log_info("WARNING: no records found for " + file_command + ", no file will be downloaded")
                file_url = ''
            else:
                logger.log_info("ERROR: generate download post response invalid; unknown error")
                file_url = ''
            self.download_ready[file_command] = True
            self.download_urls[file_command] = file_url
            if file_url:
                local_filename = file_url.split('/')[-1]
                download_file_path = os.path.join(self.download_directory, local_filename)
                # logger.log_info('Backup generated for: %s. Now start the content download' % file_command)
                # f_cmd, download_url, download_file_path, download_finished, message = await self.download_single_file(file_command, file_url, session)
                # return f_cmd, download_url, download_file_path, download_finished, message
                return file_command, file_url, download_file_path, False, "Not Attempt"
            else:
                return file_command, None, None, False, 'Invalid download URL'
        except NameError as exp:
            logger.log_info("ERROR: Empty response")
            return file_command, None, None, False, str(exp)

    def generate_download_single_file_sync(self, file_command, session):
        backup_url = 'https://secure2.chambermaster.com/directory/jsp/chamber/Backup.jsp'
        payload = {'command': file_command}
        logger.log_info("Generating backup for %s" % file_command)
        try:
            response = session.post(backup_url, data=payload, timeout=3000)
        except requests.exceptions.ConnectionError as e:
            logger.log_info("ERROR: Connection Problem. Exception: %s" % str(e))
        except requests.exceptions.Timeout as e:
            logger.log_info("ERROR: Timeout. Exception: %s" % str(e))
        except requests.exceptions.RequestException as e:
            logger.log_info("ERROR: General Error. Exception: %s" % str(e))
        except KeyboardInterrupt:
            logger.log_info("ERROR: Program exited before completing")
            exit()

        try:
            text = response.text
            if response.status_code == 200 and 'window.open("/directory/jsp/reports/Completed.jsp?' in text:
                logger.log_info("Success")
                return_string = text
                start_position = return_string.find('file=/directory/reports/2779/', 0, 200)
                end_position = return_string.find('.csv",', start_position, start_position + 200)
                file_name = return_string[start_position + 29:end_position + 4]
                if len(file_name) > 5:
                    file_url = 'https://secure2.chambermaster.com/directory/reports/2779/' + file_name
                else:
                    logger.log_info("ERROR: " + file_command + ' file name too short to be valid; aborting')
                    logger.log_info("ERROR: file name retreived was " + file_name)
                    file_url = ''
            elif 'No records were found.' in text:
                logger.log_info("WARNING: no records found for " + file_command + ", no file will be downloaded")
                file_url = ''
            else:
                logger.log_info("ERROR: generate download post response invalid; unknown error")
                file_url = ''
            self.download_ready[file_command] = True
            self.download_urls[file_command] = file_url
            if file_url:
                local_filename = file_url.split('/')[-1]
                download_file_path = os.path.join(self.download_directory, local_filename)
                # logger.log_info('Backup generated for: %s. Now start the content download' % file_command)
                # f_cmd, download_url, download_file_path, download_finished, message = await self.download_single_file(file_command, file_url, session)
                # return f_cmd, download_url, download_file_path, download_finished, message
                return file_command, file_url, download_file_path, False, "Not Attempt"
            else:
                return file_command, None, None, False, 'Invalid download URL'
        except NameError as exp:
            logger.log_info("ERROR: Empty response")
            return file_command, None, None, False, str(exp)

    async def handle_async_download_all(self, session):
        download_futures = [self.donwload_single_file_async(f_command, session) for f_command in self.file_commands]
        downloaded = {}
        for download_future in asyncio.as_completed(download_futures):
            file_command, download_url, download_file_path, download_finished, message = await download_future
            if download_url:
                logger.log_info("Download generated for: %s" % file_command)
            else:
                logger.log_info("Download could not generate for: %s" % file_command)
            if download_url:
                downloaded[file_command] = {
                    'path': download_file_path, 'download_url': download_url,
                    'status': download_finished, 'message': message
                }
        return downloaded

    def handle_download_all_sync(self, session):
        downloaded = {}
        for file_command in self.file_commands:
            file_command, download_url, download_file_path, download_finished, message = self.generate_download_single_file_sync(file_command, session)
            if download_url:
                logger.log_info("Download generated for: %s" % file_command)
            else:
                logger.log_info("Download could not generate for: %s" % file_command)
            if download_url:
                downloaded[file_command] = {
                    'path': download_file_path, 'download_url': download_url,
                    'status': download_finished, 'message': message
                }
        return downloaded

    async def perform_async_download_main(self):
        async with aiohttp.ClientSession() as session:
            auth_payload = {'UserName': self.login_username, 'Password': self.login_password}
            login = await session.post(self.login_url, data=auth_payload)
            text = await login.text()
            if login.status == 200 and 'Greater Boston Chamber of Commerce' in text:
                logger.log_info("Login Successful")
                downloaded = await self.handle_async_download_all(session)
                return downloaded
            else:
                logger.log_info('Login failed, exiting')
                sys.exit()

    def download_single_sync(self, session, download_command, url, save_path):
        try:
            if len(url) > 5:
                logger.log_info('Downloading file as sync: ' + url)
                download_file_path = save_path

                if os.path.exists(download_file_path):
                    os.remove(download_file_path)

                r = session.get(url, stream=True)
                with open(download_file_path, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)
                logger.log_info("Download finished for %s" % download_command)
                self.downloaded[download_command]['status'] = True
            else:
                logger.log_info('ERROR: download URL invalid')
        except Exception as exp:
            logger.log_info('ERROR: Unable to download file. Exception: %s' % str(exp))

    def try_download_sync(self, session, failed_downloads):
        i = 1
        for command, failed_download in failed_downloads.items():
            self.download_single_sync(session, command, failed_download['download_url'], failed_download['path'])

            logger.log_info("%s/%s items completed" % (i, len(failed_downloads)))

            i += 1

    def start_download_sync(self):
        logger.log_info("Inside start_download_sync()")
        session = self.perform_login()
        if not session:
            logger.log_info("Login Failed. Aborting...")
            sys.exit()
        count = 1
        total_files = len(self.file_commands)
        for file_command in self.file_commands:
            f_command, file_url, download_file_path, status, message = self.generate_download_single_file_sync(file_command, session)
            if file_url:
                logger.log_info("Download file generated for %s. Now start content download" % f_command)
                self.download_single_sync(session, file_command, file_url, download_file_path)
                logger.log_info("Content download completed for %s" % f_command)
            logger.log_info("%s/%s items downloaded" % (count, total_files))
            count += 1
        session.close()
        logger.log_info("start_download_sync() done")

    def start_download(self):
        logger.log_info("Inside start_download()")

        with closing(asyncio.get_event_loop()) as loop:
            self.downloaded = loop.run_until_complete(self.perform_async_download_main())
            if self.downloaded:
                logger.log_info("Download Generated.")
            else:
                logger.log_info("Download did not generate successfully")

        logger.log_info("start_download() done")

        session = self.perform_login()

        with session:

            # self.downloaded = self.handle_download_all_sync(session)
            #
            # if self.downloaded:
            #     logger.log_info("Download Generated: %s" % str(self.downloaded))
            # else:
            #     logger.log_info("Download did not generate successfully")
            #
            # logger.log_info("start_download() done")

            failed_downloads = {k:v for k, v in self.downloaded.items() if not v['status']}

            count_failed_downloads = len(failed_downloads)

            total_downloads = len(self.downloaded)

            logger.log_info("%s download generated" % (total_downloads))

            logger.log_info("----------------------------------------------------------------------------------")

            logger.log_info(str(self.downloaded))

            logger.log_info("----------------------------------------------------------------------------------")

            logger.log_info("Now %s items will be tried in squencial manner" % (count_failed_downloads))

            self.try_download_sync(session, failed_downloads)

            failed_downloads = {k: v for k, v in self.downloaded.items() if not v['status']}

            count_failed_downloads = len(failed_downloads)

            logger.log_info(
                "%s/%s items downloaded successfully" % ((total_downloads - count_failed_downloads), total_downloads))

            return count_failed_downloads == 0


if __name__ == '__main__':
    etl_downloader = ETLDownloader()
    with etl_downloader:
        etl_downloader.start_download()

