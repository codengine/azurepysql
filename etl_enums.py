from enum import Enum


class UploadStatus(Enum):
    COMPLETE = "complete"
    PARTIAL_COMPLETE = "partial_complete"
    FAILED = "failed"
    STARTED = "started"


class UploadMeta(Enum):
    UPLOAD_CHUNK_SIZE = 500


class OperatingSystem(Enum):
    WINDOWS = "WINDOWS"
    LINUX = "LINUX"
    OSX = "OSX"

