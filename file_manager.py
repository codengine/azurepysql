import os
import json
import csv
from pathlib import Path
import shutil
from etl_logger import ETLLogger
from etl_enums import UploadStatus


logger = ETLLogger.get_logger()


csv.field_size_limit(100000000)


class FileManager(object):
    def __init__(self, file_directory=os.getcwd()):
        pass

    @classmethod
    def read_all_csv_files(cls, directory):
        csv_files = []
        valid_extensions = [".csv"]
        if not os.path.exists(directory):
            return []
        for f in os.listdir(directory):
            ext = os.path.splitext(f)[1]
            if ext.lower() not in valid_extensions:
                continue
            csv_files += [f]
        return sorted(csv_files, reverse=False)

    @classmethod
    def check_if_file_unprocessed(cls, file_directory, table_name):
        csv_files = cls.read_all_csv_files(file_directory)
        for csv_file in csv_files:
            if table_name in csv_file:
                return True
        return False

    @classmethod
    def read_etl_batch_id(cls, file_path):
        batch_id = ""
        with open(file_path, "r") as f:
            batch_id = f.read()
        return batch_id

    """
    file content:
    {
        file_name: [
            ['file_name', 'table_name', 'FAILED', '1234', '{}', 'Exception message'],
            ['file_name', 'table_name', 'FAILED', '1234', '{}', 'Exception message']
        ]
    }
    """
    @classmethod
    def read_failed_rows(cls, csv_file_name, failed_row_path):
        try:
            with open(failed_row_path, "r") as ff:
                content = ff.read()
                json_content = json.loads(content)
                if json_content:
                    failed_rows = json_content.get(csv_file_name, [])
                    logger.log_info("Total %s failed rows found" % len(failed_rows))
                    logger.log_info("==============================Start Failed Rows===========================")
                    logger.log_info(str(failed_rows))
                    logger.log_info("==============================End Failed Rows===============================")
                    return failed_rows
                return []
        except Exception as exp:
            logger.log_info("Failed rows read exception. Message: %s" % str(exp))
            return []

    @classmethod
    def clear_json_file(cls, file_path):
        with open(file_path, "w", encoding='utf-8', newline='\n') as f:
            json.dump({}, f)

    @classmethod
    def save_failed_row_local(cls, csv_file_name, row, exception_message, failed_row_path, table_name, upload_id=None,
                              upload_status=UploadStatus.FAILED.value):
        try:
            new_row = [csv_file_name, table_name, upload_status, upload_id, row, exception_message]
            logger.log_info("Saving the failed row to upload later in db log")
            logger.log_info("===========================Start Failed Row===============================")
            logger.log_info(str(new_row))
            logger.log_info("===========================End Failed Row===============================")
            existing_rows = cls.read_failed_rows(csv_file_name, failed_row_path)
            if existing_rows:
                merged_rows = existing_rows + [new_row]
            else:
                merged_rows = [new_row]

            print(merged_rows)

            json_content_fresh = {
                csv_file_name: merged_rows
            }

            json_content = cls.read_json_file(failed_row_path) or json_content_fresh

            print(json_content)

            json_content[csv_file_name] = merged_rows

            print(json_content)

            with open(failed_row_path, "w", encoding='utf-8', newline='\n') as f:
                json.dump(json_content, f)

            logger.log_info("Saved the failed tow in json file: %s" % failed_row_path)

        except Exception as exp:
            logger.log_info("Save row failed in local. Exception message: %s" % str(exp))
            return False

    @classmethod
    def check_if_file_has_multiple_chunk(cls, file_directory, table_name):
        chunks = []
        csv_files = cls.read_all_csv_files(file_directory)
        for csv_file in csv_files:
            if table_name in csv_file:
                chunks += [csv_file]
                if len(chunks) > 1:
                    return True
        return len(chunks) > 1

    @classmethod
    def build_upload_tracker_file(cls, tracker_file, files, current_status={}):
        file_dict = current_status or {}
        for file in files:
            if file not in file_dict:
                file_dict[file] = False
        new_dict = {}
        for file, status in file_dict.items():
            if file in files:
                new_dict[file] = file_dict.get(file)
        file_dict = new_dict
        with open(tracker_file, "w", encoding='utf-8', newline='\n') as tf:
            json.dump(file_dict, tf)

    @classmethod
    def check_if_fresh_start(cls, tracker_file_path):
        tracker_data = cls.read_tracker_file(tracker_file_path)
        if not tracker_data:
            return True
        if all([v for k, v in tracker_data.items()]):
            return True
        return False

    @classmethod
    def check_if_all_processed_in_upload_tracker_file(cls, tracker_file_path):
        tracker_data = cls.read_tracker_file(tracker_file_path)
        if not tracker_data:
            return True
        if all([v for k, v in tracker_data.items()]):
            return True
        return False

    @classmethod
    def clear_upload_tracker_file(cls, tracker_file_path):
        file_dict = {}
        with open(tracker_file_path, "w", encoding='utf-8', newline='\n') as tf:
            json.dump(file_dict, tf)

    @classmethod
    def read_json_file(cls, file_path):
        file_data = {}
        try:
            with open(file_path, 'r', encoding='utf-8') as f:
                content = f.read()
                if content:
                    file_data = json.loads(content)
        except Exception as exp:
            logger.log_info("File read failed %s. Message: %s" % (file_path, str(exp)))
        return file_data

    @classmethod
    def read_tracker_file(cls, tracker_file):
        return cls.read_json_file(tracker_file)

    @classmethod
    def delete_file(cls, file_path):
        if os.path.exists(file_path):
            os.remove(file_path)

    @classmethod
    def remove_directory(cls, directory):
        if os.path.exists(directory):
            shutil.rmtree(directory)

    @classmethod
    def get_next_file(cls, tracker_file):
        tracker_data = cls.read_tracker_file(tracker_file)
        next_file = None
        for tf, status in tracker_data.items():
            if not status:
                next_file = tf
                break
        return next_file

    @classmethod
    def detect_main_source_file(cls, temp_file_name):
        tf_split = temp_file_name.split('_chunk')
        if tf_split:
            return tf_split[0] + '.csv'
        return None

    @classmethod
    def update_tracker_file(cls, tracker_file, file_name, status):
        tracker_data = cls.read_tracker_file(tracker_file)
        tracker_data[file_name] = status
        with open(tracker_file, "w", encoding='utf-8', newline='\n') as tf:
            json.dump(tracker_data, tf)

    @classmethod
    def get_file_name(cls, file_path):
        base_file_name = Path(file_path).name
        return base_file_name

    @classmethod
    def get_file_chunk_name_prefix(cls, file_path, file_name_postfix='chunk'):
        base_file_name = Path(file_path).name
        base_file_name = base_file_name.replace(".csv", "")
        chunk_name_prefix = base_file_name + "_" + file_name_postfix
        return chunk_name_prefix

    @classmethod
    def read_csv_file(cls, file_path):
        lines = []
        with open(file_path, newline='', encoding='iso-8859-1') as csvfile:
            source_file = csv.reader(csvfile)
            for line in source_file:
                lines += [line]
        return lines

    @classmethod
    def count_rows(cls, file_path):
        csv_data = FileManager.read_csv_file(file_path)
        row_count = len(csv_data)
        return row_count

    @classmethod
    def count_file_rows(cls, directory):
        count_dict = {

        }
        csv_files = cls.read_all_csv_files(directory)
        for file in csv_files:
            file_path = os.path.join(directory, file)
            row_count = cls.count_rows(file_path)
            row_count = row_count - 1
            if row_count < 0:
                row_count = 0
            count_dict[file] = row_count

        return count_dict

    @classmethod
    def move_file_to_archive(cls, file_path, archive_directory):
        try:
            path = Path(file_path)
            file_name = path.name
            dest_file = os.path.join(archive_directory, file_name)
            if os.path.exists(dest_file):
                os.remove(dest_file)
            if not os.path.exists(archive_directory):
                os.makedirs(archive_directory)
            shutil.move(file_path, archive_directory)
        except Exception as exp:
            print("File move failed")

    @classmethod
    def split_multiple_files(cls, file_path, output_dir, chunk_size=1000, file_name_postfix='chunk', ext=".csv"):
        base_file_name = Path(file_path).name
        base_file_name = base_file_name.replace(".csv", "")
        chunk_name_prefix = base_file_name + "_" + file_name_postfix

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        with open(file_path, newline='', encoding='iso-8859-1') as csvfile:
            source_file = csv.reader(csvfile)
            chunk = []
            c_size = 0
            chunk_file_index = 0
            row_count = 0
            for line in source_file:
                if row_count == 0:
                    row_count += 1
                    continue
                chunk += [line]
                c_size += 1
                if c_size == chunk_size:
                    chunk_file_name = chunk_name_prefix + "_" + str(chunk_file_index) + ext
                    chunk_file_path = os.path.join(output_dir, chunk_file_name)
                    with open(chunk_file_path, "w", encoding='iso-8859-1', newline='\n') as chunk_file:
                        csv_file_writer = csv.writer(chunk_file)
                        for chunk_line in chunk:
                            csv_file_writer.writerow(chunk_line)
                    chunk_file_index += 1
                    c_size = 0
                    chunk = []
                row_count += 1
            print(len(chunk))
            if chunk:
                chunk_file_name = chunk_name_prefix + "_" + str(chunk_file_index) + ext
                chunk_file_path = os.path.join(output_dir, chunk_file_name)
                with open(chunk_file_path, "w", encoding='iso-8859-1', newline='\n') as chunk_file:
                    csv_file_writer = csv.writer(chunk_file)
                    for chunk_line in chunk:
                        csv_file_writer.writerow(chunk_line)
                chunk_file_index += 1
                c_size = 0
                chunk = []


if __name__ == "__main__":
    file_path = "D:\\Projects\\AzureSQLImportPython\\azurepysql\\tmp\\cmbackup_eventRegistrationsAttendees10_chunk_115.csv"
    sfile_path = "/media/codenginebd/MyWorksandStuffs3/Projects/AzureSQLImportPython/azurepysql/tmp/cmbackup_billingTxnMap_chunk_2276.csv"

    i = 0
    csv_data = FileManager.read_csv_file("D:\\Projects\\AzureSQLImportPython\\cmbackup\\cmbackup_eventRegistrationsAttendees.csv")
    for line in csv_data:
        if i == 0:
            pass
        i += 1
    print(csv_data[len(csv_data) - 1])

    # row_count = FileManager.count_file_rows(file_path)
    #
    # print(row_count)

    # i = 0
    # with open(os.path.join(os.getcwd(), "ttest.csv"), "w") as cf:
    #     cwritter = csv.writer(cf)
    #     for csv_line in csv_data:
    #         cwritter.writerow(csv_line)
    #         if i == 1:
    #             print(csv_line)
    #             break
    #     i += 1
        # print(len(csv_line))
    # print(len(csv_data))
    # print(csv_data[18])
    # for c in csv_data[18]:
    #     print(len(c))
    # count_dict = FileManager.count_file_rows("/media/codenginebd/MyWorksandStuffs3/Projects/AzureSQLImportPython/cmbackup")
    # print(count_dict)

    # path = "/media/codenginebd/MyWorksandStuffs3/Projects/AzureSQLImportPython/test_csv/cmbackup_eventRegistrationsAttendees.csv"
    # data = FileManager.read_csv_file(file_path)
    # for i, r in enumerate(data):
    #     if i > 0:
    #         for ji, j in enumerate(r):
    #             print(len(j))
    #             # if ji == 22:
    #             #     print(len(j), j)

    # fpath = "/media/codenginebd/MyWorksandStuffs3/Projects/AzureSQLImportPython/azurepysql/failed_rows.json"
    # data = FileManager.save_failed_row_local("test.csv", "(1,2,3,4)", "Exception message", fpath, "table_name", upload_id="1234",
    #                       upload_status=UploadStatus.FAILED.value)
    # print(data)

