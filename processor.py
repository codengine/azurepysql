import os
import sys
import json
from datetime import datetime
from etl_logger import ETLLogger
from file_manager import FileManager
from dml_query_builder import DMLQueryBuilder
from query_builder import QueryBuilderModel, QueryBuilder
from db_model_util import DBModelUtil
from etl_enums import UploadStatus
from dml_query_builder import DataWranglerModel
from etl_context import ETLFileUploadContext


logger = ETLLogger.get_logger()


def try_single_upload(db_instance, insert_statement, data_row):
    # logger.log_info("Insert Data: %s" % str(data_row))

    insert_success, message = db_instance.insert_single_raw(insert_statement, data_row)
    if insert_success:
        logger.log_info("Insert successful")
    else:
        logger.log_info("Insert failed")

    return insert_success, message


def try_batch_upload(db_instance, insert_statement, data):
    batch_insert_success, batch_insert_message = db_instance.insert_many_raw(insert_statement, data)

    return batch_insert_success, batch_insert_message


def upload_to_azure_sql(db_instance, main_source_file, table_name, data, config_manager_instance):
    logger.log_info("Preparing the insert statement for %s" % table_name)

    db_table_name = table_name

    insert_table_columns = DBModelUtil.get_insert_table_columns(db_table_name)

    insert_statement = DMLQueryBuilder.prepare_dml_insert_statement(db_table_name, columns=insert_table_columns)

    # print(insert_statement)

    logger.log_info("Insert statement prepared: %s" % insert_statement)

    batch_insert_success, message = try_batch_upload(db_instance, insert_statement, data)

    if batch_insert_success:
        logger.log_info("Batch Insert successful")
    else:
        logger.log_info("Batch Insert failed")

        logger.log_info("Calculating next batch size")

        parent_batch_size = len(data)

        batch_size = parent_batch_size / 10

        batch_size = int(batch_size)

        logger.log_info("Next batch size found: %s" % batch_size)

        logger.log_info("Trying small batch with size %s" % batch_size)

        start = 0

        remaining_batch = data

        for i in range(0, parent_batch_size, batch_size):
            start = i
            end = i + batch_size
            small_batch_data = data[start:end]

            logger.log_info("Processing %s - %s out of %s" % (start + 1, end, parent_batch_size))

            small_batch_success, small_batch_message = try_batch_upload(db_instance,
                                                                        insert_statement,
                                                                        small_batch_data)
            if small_batch_success:
                remaining_batch = data[end:]
            else:
                logger.log_info("Batch %s - %s failed" % (start + 1, end))

                logger.log_info("Now trying row level insert for each row")

                for j, row in enumerate(small_batch_data):
                    insert_success, insert_message = try_single_upload(db_instance, insert_statement, row)
                    if not insert_success:
                        logger.log_info("Row insert failed: %s" % str(row))
                        # Now log to the database table
                        d, fname, failed_row_json_file_path = config_manager_instance.read_failed_log_json_file_path()
                        ld, lfname, lfpath = config_manager_instance.read_lock_file_path()
                        upload_id = FileManager.read_etl_batch_id(lfpath)

                        logger.log_info(row)

                        FileManager.save_failed_row_local(main_source_file, row, insert_message,
                                                          failed_row_json_file_path, table_name, upload_id=upload_id,
                                                          upload_status=UploadStatus.FAILED.value)
                        logger.log_info("Row saved in local for later upload in the SQL log file")
                    else:
                        logger.log_info("Row insert successful")

                    remaining_batch = data[end + j:]


def get_csv_file_row_count_from_tracker(config, csv_file_name):
    row_count_file_dir, row_count_file_name, row_count_file_path = config.read_row_count_file_path()
    with open(row_count_file_path, "r") as rf:
        content = rf.read()
        if content:
            row_count_json = json.loads(content)
            return row_count_json.get(csv_file_name, -1)
    return -1


def get_uploaded_data_row_count(db_instance, csv_file_name):
    db_table_name = DBModelUtil.detect_table_name(csv_file_name)

    select_query = QueryBuilder.build_count_query(table_name=db_table_name)
    data = db_instance.execute_query(select_query)
    if data:
        return data[0][0]
    return 0


def process_temp_files(db_instance, main_source_file, temp_directory, temp_files,
                       config_manager_instance):
    logger.log_info("Detect the db table name for %s" % main_source_file)

    db_table_name = DBModelUtil.detect_table_name(main_source_file)

    if db_table_name:
        logger.log_info("DB table name found: %s" % db_table_name)
    else:
        logger.log_info("No associated db table name found for %s" % main_source_file)

    if db_table_name:

        for temp_file in temp_files:
            temp_file_path = os.path.join(temp_directory, temp_file)
            data_rows = FileManager.read_csv_file(temp_file_path)

            logger.log_info("Cleaning data to be uploaded in Azure SQL")

            cleaned_rows, failed_rows = DMLQueryBuilder.clean_rows(db_table_name, data_rows)

            logger.log_info("Data cleaned %s out of %s" % (len(cleaned_rows), len(data_rows)))

            logger.log_info("Failed to clean rows: %s" % str(failed_rows))

            upload_to_azure_sql(db_instance, main_source_file, db_table_name, cleaned_rows, config_manager_instance)

            archive_directory = os.path.join(temp_directory, "archive", datetime.now().strftime("%d_%m_%Y_%H"))
            if not os.path.exists(archive_directory):
                os.makedirs(archive_directory)

            FileManager.move_file_to_archive(temp_file_path, archive_directory)


def update_upload_status(db_instance, table_name, status):
    update_query = DMLQueryBuilder.prepare_update_statement(DBModelUtil.upload_status,
                                                            fields=['upload_status'],
                                                            values=["'%s'" % status],
                                                            conditions={
                                                                "table_name": "'%s'" % table_name
                                                            })
    logger.log_info("Updating upload status for %s" % table_name)
    updated = db_instance.execute_dml_query(update_query)
    if updated:
        logger.log_info("Upload status updated successfully")
    else:
        logger.log_info("Upload status update failed")


def update_upload_complete_time(db_instance, table_name):
    now_dt = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.0")
    update_query = DMLQueryBuilder.prepare_update_statement(DBModelUtil.upload_status,
                                                            fields=['upload_complete_time'],
                                                            values=["'" + now_dt + "'"],
                                                            conditions={
                                                                "table_name": "'%s'" % table_name
                                                            })
    logger.log_info("Updating upload status for %s" % table_name)
    updated = db_instance.execute_dml_query(update_query)
    if updated:
        logger.log_info("Upload status updated successfully")
    else:
        logger.log_info("Upload status update failed")


def update_etl_file_upload_status(db_instance, source_file, config_instance, **kwargs):
    bd, bfname, bfpath = config_instance.read_lock_file_path()
    batch_id = FileManager.read_etl_batch_id(bfpath)
    total_rows_count = get_csv_file_row_count_from_tracker(config_instance, source_file)
    uploaded_data_row_count = get_uploaded_data_row_count(db_instance, source_file)

    upload_status = UploadStatus.COMPLETE.value
    if total_rows_count != uploaded_data_row_count:
        upload_status = UploadStatus.PARTIAL_COMPLETE.value

    rows_failed = total_rows_count - uploaded_data_row_count

    rows_failed = rows_failed if rows_failed > 0 else 0

    print("Rows faield: %s" % rows_failed)

    upload_status_update_query = DMLQueryBuilder.prepare_upload_status_update_query(batch_id=batch_id,
                                                                                    file_name=source_file,
                                                                                    reason=None,
                                                                                    upload_status=upload_status,
                                                                                    rows_uploaded=uploaded_data_row_count,
                                                                                    rows_failed=0,
                                                                                    upload_complete_time=datetime.utcnow())
    logger.log_debug("Update status query for table: %s: %s" % (source_file, upload_status_update_query))
    execute_status, exception_message = db_instance.execute_dml_query(upload_status_update_query)
    if execute_status:
        logger.log_info("upload status updated")
    else:
        logger.log_info("upload status updated failed. Exiting now...")
        exit()


def insert_etl_file_update_status(db_instance, source_file, config_instance, status=UploadStatus.STARTED.value):
    bd, bfname, bfpath = config_instance.read_lock_file_path()
    batch_id = FileManager.read_etl_batch_id(bfpath)
    total_rows_count = get_csv_file_row_count_from_tracker(config_instance, source_file)
    insert_query = DMLQueryBuilder.prepare_upload_status_insert_query(batch_id, source_file,
                                                                      total_rows_count,
                                                                      upload_start_time=datetime.utcnow(),
                                                                      upload_status=status)
    logger.log_debug("Insert update status for table %s: %s" % (source_file, insert_query))
    execute_status, exception_message = db_instance.execute_dml_query(insert_query)
    if execute_status:
        logger.log_info("upload status updated")
    else:
        logger.log_info("upload status updated failed. Exiting now...")
        exit()


def handle_post_processing_upload(db_instance, csv_files_directory, temp_directory, tracker_file_path,
                                  archive_directory,config_manager_instance, file_processed):
    csv_file = os.path.join(csv_files_directory, file_processed)

    logger.log_info("File %s processed" % file_processed)

    logger.log_info("Checking the row count for %s" % file_processed)

    upload_status = UploadStatus.COMPLETE.value

    uploaded_data_row_count = 0

    reason = None

    csv_file_row_count = get_csv_file_row_count_from_tracker(config_manager_instance, file_processed)

    if csv_file_row_count < 0:
        logger.log_info("Row count not found in the tracker to verify. Cannot be verified")

        reason = "NO CSV rows found"

    else:
        logger.log_info("Total CSV file data row count: %s" % csv_file_row_count)

        logger.log_info("Getting the uploaded data row count for %s" % file_processed)

        uploaded_data_row_count = get_uploaded_data_row_count(db_instance, file_processed)

        if uploaded_data_row_count != -1:
            logger.log_info("Total %s rows uploaded for %s" % (uploaded_data_row_count, file_processed))
            if uploaded_data_row_count == csv_file_row_count:
                upload_status = UploadStatus.COMPLETE.value
            else:
                upload_status = UploadStatus.PARTIAL_COMPLETE.value
                reason = "Not all rows uploaded"
        else:
            logger("No data rows found from the server to verify")

            reason = "No data uploaded"

            if csv_file_row_count == 0:
                logger.log_info("CSV file was empty. No data upload occurred")

    logger.log_info("Updating the tracker file")
    FileManager.update_tracker_file(tracker_file_path, file_processed, status=True)

    logger.log_info("Updating the upload status for %s with %s" % (file_processed, upload_status))

    update_etl_file_upload_status(db_instance, file_processed, config_manager_instance)

    logger.log_info("Moving file to archive")

    bd, bfname, bfpath = config_manager_instance.read_lock_file_path()
    batch_id = FileManager.read_etl_batch_id(bfpath)

    batch_archive_directory = os.path.join(archive_directory, str(batch_id))

    FileManager.move_file_to_archive(csv_file, batch_archive_directory)

    logger.log_info("File moved to archive")


def handle_file_processing(db_instance, csv_files_directory, temp_directory, tracker_file_path, archive_directory,
                           config_manager_instance):
    logger.log_info("Checking if the file already split and left unprocessed")
    temp_files = FileManager.read_all_csv_files(directory=temp_directory)
    if temp_files:
        logger.log_info("Pending unprocessed files detected: %s" % len(temp_files))

        logger.log_info("Detect the main file")
        source_file = FileManager.detect_main_source_file(temp_files[0])
        if source_file:
            logger.log_info("Source file detected: %s" % source_file)

            process_temp_files(db_instance, source_file, temp_directory, temp_files, config_manager_instance)

            temp_files = FileManager.read_all_csv_files(directory=temp_directory)
            if not temp_files:
                handle_post_processing_upload(db_instance, csv_files_directory, temp_directory, tracker_file_path,
                                              archive_directory,
                                              config_manager_instance, source_file)

            else:
                logger.log_info("All files not processed. Exiting...")
                return
        else:
            logger.log_info("No source file detected. Skipping to the next")

    logger.log_info("No more pending unprocessed files found. Taking the next unprocessed file")

    next_file = FileManager.get_next_file(tracker_file_path)
    while next_file:

        with ETLFileUploadContext(db_instance=db_instance, csv_file=next_file,
                                  config_manager_instance=config_manager_instance) as fuc:

            logger.log_info("Delete all the existing data from %s" % next_file)

            db_table_name = DBModelUtil.detect_table_name(next_file)

            delete_statement = DMLQueryBuilder.prepare_delete_all_statement(db_table_name)

            logger.log_info("Executing delete statement: %s" % delete_statement)

            deleted = db_instance.execute_dml_query(delete_statement)
            if deleted:
                logger.log_info("All rows deleted")
            else:
                logger.log_info("All rows could not be deleted")

            logger.log_info("Updating upload status for this file %s" % next_file)
            delete_statement = DMLQueryBuilder.prepare_delete_statement(
                DBModelUtil.upload_status,
                conditions={"table_name": "'%s'" % next_file})

            db_instance.execute_dml_query(delete_statement)

            insert_etl_file_update_status(db_instance, next_file, config_manager_instance)

            logger.log_info("Splitting %s into multiple files" % next_file)

            csv_file = os.path.join(csv_files_directory, next_file)

            FileManager.split_multiple_files(csv_file, temp_directory)

            temp_files = FileManager.read_all_csv_files(directory=temp_directory)

            logger.log_info("File split into %s " % len(temp_files))

            process_temp_files(db_instance, next_file, temp_directory, temp_files, config_manager_instance)

            temp_files = FileManager.read_all_csv_files(directory=temp_directory)
            if not temp_files:
                handle_post_processing_upload(db_instance, csv_files_directory, temp_directory, tracker_file_path,
                                              archive_directory,
                                              config_manager_instance, next_file)

            else:
                logger.log_info("All files not processed. Exiting...")
                sys.exit()
        next_file = FileManager.get_next_file(tracker_file_path)

    logger.log_info("Files processed")