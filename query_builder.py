class QueryBuilderModel(object):
    @classmethod
    def select_fields(cls, table_name):
        fields = {
            'chart_of_accounts': [
                'id', 'acc_id', 'name', 'type', 'description', 'cc_id', 'is_active', 'is_un_deposited_fund',
                'account_code_id', 'date_created', 'last_updated'
            ]
        }
        return fields.get(table_name, [])


class QueryBuilder(object):
    @classmethod
    def build_select_query(cls, table_name, fields=[], exclude_fields=[]):
        if exclude_fields:
            fields = [f for f in fields if f not in exclude_fields]

        select_fields = ", ".join(fields)
        return "SELECT %s FROM %s" % (select_fields, table_name)

    @classmethod
    def build_count_query(cls, table_name):
        return "SELECT COUNT(*) as TOTAL FROM %s" % table_name
